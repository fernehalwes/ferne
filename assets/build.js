const esbuild = require("esbuild");
const fs = require("fs");
const path = require("path");
const argv = require("minimist")(process.argv.slice(2));

const bundle = true;
const logLevel = process.env.ESBUILD_LOG_LEVEL || "silent";
const watch = !!process.env.ESBUILD_WATCH;

// Get all JS(X) files in the root of the js/ directory
const dirstat = fs.readdirSync("./js/");

const p = esbuild
  .build({
    entryPoints: dirstat
      .filter((d) => [".js", ".jsx"].includes(path.extname(d)))
      .map((d) => path.resolve("./js/", d)),
    bundle,
    define: {
      global: "window",
    },
    target: "es2016",
    outdir: "../priv/static/assets",
    loader: {
      ".woff2": "dataurl"
    },
    minify:
      argv.analyze ||
      argv.metafile ||
      (process.env.MIX_ENV &&
        ["prod", "staging"].includes(process.env.MIX_ENV)),
    logLevel,
    sourcemap:
      !argv.analyze && !argv.metafile && !process.env.MIX_ENV && "inline",
    watch,
    // jsxFactory: "h",
    // jsxFragment: "Fragment",
    metafile: argv.analyze || argv.metafile,
  })
  .then((res) => {
    if (argv.analyze) {
      esbuild.analyzeMetafile(res.metafile).then((text) => {
        console.log(text);
      });
    }

    if (argv.metafile) {
      require("fs").writeFileSync("meta.json", JSON.stringify(res.metafile));
    }
  });
fs.mkdirSync("../priv/static/assets/fonts", { recursive: true });
fs.copyFileSync(
  "../node_modules/bootstrap-icons/font/fonts/bootstrap-icons.woff",
  "../priv/static/assets/fonts/bootstrap-icons.woff"
);
fs.copyFileSync(
  "../node_modules/bootstrap-icons/font/fonts/bootstrap-icons.woff2",
  "../priv/static/assets/fonts/bootstrap-icons.woff2"
);
fs.copyFileSync(
  "../node_modules/bootstrap-icons/font/bootstrap-icons.css",
  "../priv/static/assets/bootstrap-icons.css"
);

if (watch) {
  p.then((_res) => {
    process.stdin.on("close", () => {
      process.exit(0);
    });

    process.stdin.resume();
  });
}
