import ClipboardJS from "clipboard";
import Toastify from "toastify-js";

const clipboard = new ClipboardJS("#copyToClipboard");
clipboard.on("success", () => {
  Toastify({
    duration: 3000,
    close: true,
    stopOnFocus: false,
    position: "right",
    text: "Link copied to clipboard!",
  });
});
