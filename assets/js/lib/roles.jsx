import React from "react";
import flatten from "lodash.flatten";

const TANK = ["gunbreaker", "warrior", "darkknight", "paladin"];
const HEALER = [
  "sage",
  "whitemage",
  "astrologian",
  "scholar",
];
const RGHEALER = ["whitemage", "astrologian"];
const SHHEALER = ["sage", "scholar"];
const DPS = [
  "monk",
  "reaper",
  "ninja",
  "samurai",
  "dragoon",
  "bard",
  "dancer",
  "machinist",
  "blackmage",
  "redmage",
  "summoner",
];
const MELEE = ["reaper", "monk", "ninja", "samurai", "dragoon"];
const PHYSRANGED = ["bard", "dancer", "machinist"];
const MGRANGED = ["blackmage", "summoner", "redmage"];

export function formatRoles(roles) {
  if (!roles) return "";
  const split = roles.split(",");
  return split.map((i) => (
    <img
      key={i}
      src={`/images/classes/${i}.png`}
      width="24"
      style={{ marginBottom: 3 }}
      height="24"
    />
  ));
}

export function expandRoles(roles) {
  return flatten(
    roles.split(",").map(role => {
      switch (role) {
        case "tank":
          return TANK;
        case "healer":
          return HEALER;
        case "rghealer":
          return RGHEALER;
        case "shhealer":
          return SHHEALER;
        case "dps":
          return DPS;
        case "melee":
          return MELEE;
        case "physranged":
          return PHYSRANGED;
        case "magicranged":
          return MGRANGED;
        default:
          return role;
      }
    })
  )
}

export function formatRole(role, ratio = 24) {
  return `<img src="/images/classes/${role}.png" width="${ratio}" style="margin-bottom: 5px" height="${ratio}" />`;
}

export function formatRoleJsx(role, ratio = 24) {
  return (
    <img src={`/images/classes/${role}.png`} width={ratio} height={ratio} />
  );
}

export function is_tank(role) {
  return TANK.concat(["tank"]).includes(
    role
  );
}

export function is_healer(role) {
  return HEALER.concat([
    "rghealer",
    "shhealer",
    "healer",
  ]).includes(role);
}

export function is_dps(role) {
  return DPS.concat([
    "dps",
    "physranged",
    "magicranged",
    "melee",
  ]).includes(role);
}

export function is_shhealer(role) {
  return SHHEALER.concat(["shhealer"]).includes(role);
}

export function is_rghealer(role) {
  return RGHEALER.concat(["rghealer"]).includes(role);
}

export function sortByRole(att1, att2) {
  const is_melee = (role) =>
    MELEE.concat(["melee"]).includes(role);
  const is_physranged = (role) =>
    PHYSRANGED.concat(["physranged"]).includes(role);
  const is_magicranged = (role) =>
    MGRANGED.concat(["magicranged"]).includes(role);
  if (att1.custom_data.party.role && att2.custom_data.party.role) {
    const role1 = att1.custom_data.party.role;
    const role2 = att2.custom_data.party.role;
    // 1: Tanks are above all else
    if (is_tank(role1) && !is_tank(role2)) return -1;
    if (is_tank(role2) && !is_tank(role1)) return 1;
    // 2: Healers above DPS
    if (is_healer(role1) && !is_healer(role2)) return -1;
    if (is_healer(role2) && !is_healer(role1)) return 1;
    // 3: Regen healers above shield healers
    if (is_rghealer(role1) && is_shhealer(role2)) return -1;
    if (is_rghealer(role2) && is_shhealer(role1)) return 1;
    // 4: Melee above all other DPS
    if (is_melee(role1) && !is_melee(role2)) return -1;
    if (is_melee(role2) && !is_melee(role1)) return 1;
    // 5: Phys. ranged above magic ranged
    if (is_physranged(role1) && !is_physranged(role2)) return -1;
    if (is_physranged(role2) && !is_physranged(role1)) return 1;
    return att1.ign > att2.ign ? 1 : -1;
  } else {
    if (att1.ign > att2.ign) {
      return 1;
    }
    return -1;
  }
}
