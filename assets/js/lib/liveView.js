import { Socket } from "phoenix";
import { LiveSocket } from "phoenix_live_view";
import Toastify from "toastify-js";

export default function (hooks = {}, metadata = {}) {
  let csrfToken = document
    .querySelector("meta[name='csrf-token']")
    .getAttribute("content");
  let hooksWithToast = {
    Toast: {
      mounted() {
        window.globalTippies.forEach((tippy) => tippy.destroy());
        window.globalTippies = window.tippy("[data-tippy-content]");
        this.handleEvent("showToast", (opts) => {
          Toastify({
            duration: 3000,
            close: true,
            stopOnFocus: false,
            position: "right",
            ...opts,
          }).showToast();
        });
        this.handleEvent("refreshTippy", () => {
          window.globalTippies.forEach((tippy) => tippy.destroy());
          window.globalTippies = window.tippy("[data-tippy-content]");
        });
        this.handleEvent("readLocalStorage", ({ key: key }) => {
          const res = window.localStorage.getItem(`ferne-${key}`);
          this.pushEvent("readLocalStorageComplete", JSON.parse(res));
        });
        this.handleEvent("writeLocalStorage", ({ key: key, data: data }) => {
          window.localStorage.setItem(`ferne-${key}`, JSON.stringify(data));
        });
      },
    },
    ...hooks,
  };
  let liveSocket = new LiveSocket("/live", Socket, {
    params: { _csrf_token: csrfToken },
    hooks: hooksWithToast,
    metadata,
  });
  liveSocket.connect();
  window.liveSocket = liveSocket;
}
