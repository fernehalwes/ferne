// A little script that hides the signup form and disables the button when
// it's known that someone has already signed up for this event (via the client-side
// cookie).

import classnames from "clsx";
import React, { useState } from "react";
import { createRoot } from "react-dom/client";

const potential_cookie = document.cookie
  .split("; ")
  .find((row) => row.startsWith("_ferne_current_events="));

let hasMatch = false;
if (potential_cookie) {
  const url = new URL(document.URL);
  const re = /"([\w-]{36})"/g;
  const id = url.pathname.split("/")[2];
  const value = potential_cookie.split("=")[1];
  let match;
  while ((match = re.exec(value))) {
    if (match[1] === id) {
      hasMatch = true;
      const button = document.getElementById("signup-button");
      const form = document.getElementById("signup-form");
      const header = document.getElementById("signup");
      const alert = document.getElementById("signupAlert");
      button.className += " disabled";
      button.innerHTML = "Already signed up!";
      form.setAttribute("style", "display: none;");
      header.setAttribute("style", "display: none;");
      alert.removeAttribute("class");
    }
  }
}

if (!hasMatch) {
  const potential_mount = document.getElementById("roles-mount");
  if (potential_mount) {
    mountComponent(potential_mount);
  }
}

function mountComponent(mount) {
  createRoot(mount).render(
    <App
      roles={mount.dataset["roleParams"] || mount.dataset["existingRoles"]}
      rolesOnly={mount.dataset["rolesOnly"] == ""}
      restrictRoles={mount.dataset["restrictRoles"] || "all"}
    />
  );
}

function App(props) {
  const { roles, rolesOnly, restrictRoles } = props;
  const [sel, setSel] = useState(roles ? roles.split(",") : []);
  const addOrDel = (id) => {
    if (sel.includes(id)) {
      setSel((sel) => sel.filter((s) => s != id));
    } else if (sel.length < 4) {
      setSel((sel) => [...sel, id]);
    }
  };

  return (
    <div className="row">
      <input
        type="hidden"
        value={Object.values(sel).join(",")}
        name="attendee[ffxiv_roles]"
      />
      <small className="text-muted">
        Select up to 4 classes and/or roles that you prefer playing in this
        event, prioritized by the order you select them in.
      </small>
      {rolesOnly && (
        <small className="text-muted">
          The event organizers have opted for restricting selections to roles
          instead of classes.
        </small>
      )}
      <a role="button" className="link mb-2" onClick={() => setSel([])}>
        Unselect all
      </a>
      <div className="col-12 col-lg-6 row">
        <div className={restrictRoles === "all" && "col"}>
          {!["healers", "dps"].includes(restrictRoles) && (
            <div className="list-group list-group-flush">
              {genHeaderRole(sel, addOrDel, "tank", "Tanks", props)}
              {genRole(sel, addOrDel, "paladin", "Paladin", "tank", props)}
              {genRole(sel, addOrDel, "warrior", "Warrior", "tank", props)}
              {genRole(
                sel,
                addOrDel,
                "darkknight",
                "Dark Knight",
                "tank",
                props
              )}
              {genRole(
                sel,
                addOrDel,
                "gunbreaker",
                "Gunbreaker",
                "tank",
                props
              )}
            </div>
          )}
          {!["tanks", "dps"].includes(restrictRoles) && (
            <>
              <div className="list-group mt-2 list-group-flush">
                {genHeaderRole(
                  sel,
                  addOrDel,
                  "rghealer",
                  "Regen Healer",
                  props
                )}
                {genRole(
                  sel,
                  addOrDel,
                  "whitemage",
                  "White Mage",
                  "rghealer",
                  props
                )}
                {genRole(
                  sel,
                  addOrDel,
                  "astrologian",
                  "Astrologian",
                  "rghealer",
                  props
                )}
              </div>

              <div className="list-group mt-2 list-group-flush">
                {genHeaderRole(
                  sel,
                  addOrDel,
                  "shhealer",
                  "Shield Healer",
                  props
                )}
                {genRole(
                  sel,
                  addOrDel,
                  "scholar",
                  "Scholar",
                  "shhealer",
                  props
                )}
                {genRole(sel, addOrDel, "sage", "Sage", "shhealer", props)}
              </div>
            </>
          )}
        </div>
        <div className="col">
          {!["tanks", "healers"].includes(restrictRoles) && (
            <>
              <div className="list-group list-group-flush">
                {genHeaderRole(sel, addOrDel, "melee", "Melee", props)}
                {genRole(sel, addOrDel, "monk", "Monk", "melee", props)}
                {genRole(sel, addOrDel, "dragoon", "Dragoon", "melee", props)}
                {genRole(sel, addOrDel, "ninja", "Ninja", "melee", props)}
                {genRole(sel, addOrDel, "samurai", "Samurai", "melee", props)}
                {genRole(sel, addOrDel, "reaper", "Reaper", "melee", props)}
              </div>
              <div className="list-group mt-2 list-group-flush">
                {genHeaderRole(
                  sel,
                  addOrDel,
                  "physranged",
                  "Phys. Ranged",
                  props
                )}
                {genRole(sel, addOrDel, "bard", "Bard", "physranged", props)}
                {genRole(
                  sel,
                  addOrDel,
                  "machinist",
                  "Machinist",
                  "physranged",
                  props
                )}
                {genRole(
                  sel,
                  addOrDel,
                  "dancer",
                  "Dancer",
                  "physranged",
                  props
                )}
              </div>
              <div className="list-group mt-2 list-group-flush">
                {genHeaderRole(
                  sel,
                  addOrDel,
                  "magicranged",
                  "Mg. Ranged",
                  props
                )}
                {genRole(
                  sel,
                  addOrDel,
                  "blackmage",
                  "Black Mage",
                  "magicranged",
                  props
                )}
                {genRole(
                  sel,
                  addOrDel,
                  "summoner",
                  "Summoner",
                  "magicranged",
                  props
                )}
                {genRole(
                  sel,
                  addOrDel,
                  "redmage",
                  "Red Mage",
                  "magicranged",
                  props
                )}
              </div>
            </>
          )}
        </div>
      </div>
    </div>
  );
}

function genHeaderRole(sel, addOrDel, id, name, props, subroles = []) {
  const headerClasses = (id) =>
    classnames(
      [
        "list-group-item",
        "fw-bold",
        "d-flex",
        "fs-5",
        "align-items-center",
        "justify-content-between",
        "user-select-none",
        "cursor-pointer",
      ],
      {
        "text-white": sel.includes(id),
        "bg-dark": sel.includes(id),
      }
    );

  function onClick() {
    if (!sel.includes(getRole(id))) {
      addOrDel(id);
    }
  }

  return (
    <div className={headerClasses(id)} onClick={onClick}>
      <div className="d-flex align-items-center">
        {genImgTag(id, name, 20)}
        <span className="ms-2">{name}</span>
      </div>
      <div className="fs-6 fw-normal">
        {sel.includes(id) && `${sel.indexOf(id) + 1}.`}
      </div>
    </div>
  );
}

function genRole(sel, addOrDel, id, name, group, props) {
  const divClass = classnames(
    [
      "list-group-item",
      "ps-4",
      "d-flex",
      "py-1",
      "cursor-pointer",
      "justify-content-between",
      "user-select-none",
    ],
    {
      disabled: props.rolesOnly,
      "text-white": sel.includes(id) || sel.includes(group),
      "bg-dark": sel.includes(id) || sel.includes(group),
    }
  );

  function onClick() {
    if (!sel.includes(getRole(id))) {
      addOrDel(id);
    }
  }

  if (props.rolesOnly) {
    return;
  }

  return (
    <div className={divClass} onClick={onClick}>
      <div className="d-flex align-items-center">
        {genImgTag(id, name, 20)}
        <span className="ms-2">{name}</span>
      </div>
      <div>{sel.includes(id) && `${sel.indexOf(id) + 1}.`}</div>
    </div>
  );
}

function getRole(id) {
  if (["paladin", "warrior", "darkknight", "gunbreaker"].includes(id))
    return "tank";
  if (["whitemage", "astrologian"].includes(id)) return "rghealer";
  if (["scholar", "sage"].includes(id)) return "shhealer";
  if (["monk", "samurai", "dragoon", "ninja", "reaper"].includes(id))
    return "melee";
  if (["bard", "dancer", "machinist"].includes(id)) return "physranged";
  if (["blackmage", "summoner", "redmage"].includes(id)) return "magicranged";
  return false;
}

function genImgTag(id, name, width = 50) {
  return <img src={`/images/classes/${id}.png`} width={width} alt={name} />;
}
