import React, { createContext, useState, useEffect } from "react";
import { createRoot } from "react-dom/client";
import { Socket } from "phoenix";
import createColor from "create-color";
import contrast from "contrast";
import groupBy from "lodash.groupby";

import "ag-grid-community/dist/styles/ag-grid.css";
import "ag-grid-community/dist/styles/ag-theme-alpine.css";
import "reoverlay/lib/ModalWrapper.css";

import Grid from "./components/grid";
import Parties from "./components/parties";
import { ModalContainer, Reoverlay } from "reoverlay";
import RoleSelect from "./components/role_select";
import NewParty from "./components/new_party";
import DisplayParties from "./components/display_parties";
import Tutorial from "./components/manage_attendees_tutorial";
import { Popover } from "./components/popover";
import GridMobile from "./components/grid_mobile";

export const Context = createContext({});

const App = () => {
  const [data, setData] = useState({ event: { attendees: [] } });
  const [listGroups, setListGroups] = useState({
    worry: [],
    black: [],
    favor: [],
  });
  const [gridApi, setGridApi] = useState(null);
  // const [columnApi, setColumnApi] = useState(null);
  const [reassigning, setReassigning] = useState(false);
  const [reassigningAtt, setReassigningAtt] = useState(null);
  const [selectedAttendees, setSelectedAttendees] = useState([]);
  const [width, setWidth] = useState(window.innerWidth);
  const [blockData, setBlockData] = useState({
    hasParties: false,
    hasTitles: false,
    parties: [],
    customFields: [],
  });
  const [channel, setChannel] = useState(null);
  const [partiesShown, setPartiesShown] = useState(false);

  useEffect(() => {
    window.addEventListener("resize", handleWindowSizeChange);
    const csrfToken = document
      .querySelector("meta[name='csrf-token']")
      .getAttribute("content");
    const sock = new Socket("/event", {
      params: { _csrf_token: csrfToken, userId: window.userId },
    });
    sock.connect();

    const chan = sock.channel(`manage:${window.eventId}`);
    chan.join().receive("ok", (data) => {
      setListGroups(groupBy(data.lists, (d) => d.type));
      setData(data);
      setBlockData(assembleBlockData(data.event));
      setChannel(chan);
      const hasSeenTutorial = localStorage.getItem("ferne:manage_tutorial");
      if (hasSeenTutorial === null) {
        Reoverlay.showModal(Tutorial);
      }
    });

    return () => {
      window.removeEventListener("resize", handleWindowSizeChange);
    };
  }, []);

  function handleWindowSizeChange() {
    setWidth(window.innerWidth);
  }

  if (width <= 768) {
    document.getElementById("grid").classList.remove("grid-height");
    return (
      <Context.Provider
        value={{
          listGroups,
        }}
      >
        <GridMobile data={data} blockData={blockData} />
      </Context.Provider>
    );
  } else {
  }

  document.getElementById("grid").classList.add("grid-height");
  return (
    <Context.Provider
      value={{
        listGroups,
        setReassigning,
        reassigning,
        reassigningAtt,
        setReassigningAtt,
        assignToPartyWithRole,
        highlightInGrid,
        removeAssignment,
        toggleLeaderState,
        deleteParty,
        isMobile: width <= 768,
      }}
    >
      <div className="d-flex mb-1 align-items-center justify-content-between">
        <div className="d-flex align-items-center">
          {data.edit_permission && (
            <Popover
              placement="right"
              render={({ close, labelId, descriptionId }) => (
                <div className="card p-2">
                  <button
                    className="btn btn-sm btn-outline-primary mb-1"
                    disabled={selectedAttendees.length === 0}
                    onClick={confirmSelected}
                  >
                    Confirm selected
                  </button>
                  <button
                    className="btn btn-sm btn-outline-primary mb-1"
                    disabled={selectedAttendees.length === 0}
                    onClick={unconfirmSelected}
                  >
                    Unconfirm selected
                  </button>
                  {blockData.hasParties && (
                    <button
                      className="btn btn-sm btn-outline-success mb-1"
                      onClick={confirmAllInParty}
                    >
                      Confirm all party members
                    </button>
                  )}
                </div>
              )}
            >
              <button className="btn btn-sm btn-outline-primary me-1">
                <i className="bi bi-gear"></i>
              </button>
            </Popover>
          )}
          <div>
            {data.event.attendee_limit && (
              <div className="fs-6 align-middle">
                <span className="text-primary fw-bold">
                  {data.event.attendees.filter((a) => a.selected).length}
                </span>
                /{data.event.attendee_limit} attendees confirmed
              </div>
            )}
          </div>
          {data.edit_permission && <div></div>}
        </div>
        <div>
          {blockData.hasParties && (
            <>
              <button
                className="btn btn-outline-success me-1"
                onClick={displayParties}
              >
                <i className="bi bi-table"></i>
              </button>
              {data.edit_permission && (
                <button
                  className="btn btn-outline-success me-1"
                  onClick={createNewParty}
                >
                  <i className="bi bi-plus"></i>
                  Add party
                </button>
              )}
              <button
                className="btn btn-outline-primary"
                onClick={() => setPartiesShown(!partiesShown)}
              >
                {partiesShown ? "Hide" : "Show"} parties
              </button>
            </>
          )}
        </div>
      </div>
      <div className="row h-100">
        <div className={partiesShown ? "col-8" : "col"}>
          <Grid
            data={data}
            setData={setData}
            setGridApi={setGridApi}
            // setColumnApi={setColumnApi}
            blockData={blockData}
            channel={channel}
            setSelectedAttendees={setSelectedAttendees}
          />
        </div>
        {partiesShown && (
          <div className="col-4 h-100 scroll">
            <Parties
              data={data}
              setData={setData}
              blockData={blockData}
              channel={channel}
              gridApi={gridApi}
            />
          </div>
        )}
      </div>
      <ModalContainer />
    </Context.Provider>
  );

  function confirmSelected() {
    channel
      .push("confirm", { attIds: selectedAttendees.map((a) => a.id) })
      .receive("ok", (payload) => {
        setData({ ...data, event: payload });
      });
  }
  function unconfirmSelected() {
    channel
      .push("unconfirm", { attIds: selectedAttendees.map((a) => a.id) })
      .receive("ok", (payload) => {
        const rowNodes = selectedAttendees.map((a) => gridApi.getRowNode(a.id));
        gridApi.redrawRows({ rowNodes });
        setData({ ...data, event: payload });
      });
  }
  function confirmAllInParty() {
    channel.push("confirm_all_in_party", {}).receive("ok", (payload) => {
      setData({ ...data, event: payload });
    });
  }

  function displayParties() {
    Reoverlay.showModal(DisplayParties, {
      data,
      blockData,
    });
  }

  function assignToPartyWithRole(att, party) {
    setReassigning(false);
    setReassigningAtt(null);
    if (blockData.hasFfxivRoles) {
      Reoverlay.showModal(RoleSelect, {
        attendee: att,
        partyName: party.name,
        reassigningAtt,
        blockData,
        onComplete(role) {
          Reoverlay.hideModal();
          channel
            .push("set_custom_data_field", {
              uid: att.id,
              field: "party",
              value: {
                id: party.id,
                role,
                leader:
                  !!att.custom_data.party && !!att.custom_data.party.leader,
              },
            })
            .receive("ok", (payload) => {
              setData({ ...data, event: payload });
            });
        },
      });
    } else {
      channel.push("set_custom_data_field", {
        uid: att.id,
        field: "party",
        value: {
          id: party.id,
          leader: !!att.custom_data.party && !!att.custom_data.party.leader,
        },
      });
    }
  }

  function removeAssignment(att) {
    channel
      .push("set_custom_data_field", {
        uid: att.id,
        field: "party",
        value: { id: null, role: null },
      })
      .receive("ok", (payload) => {
        setData({ ...data, event: payload });
      });
  }

  function toggleLeaderState(att) {
    channel
      .push("set_custom_data_field", {
        uid: att.id,
        field: "party",
        value: {
          ...att.custom_data.party,
          leader: !att.custom_data.party.leader,
        },
      })
      .receive("ok", (payload) => {
        setData({ ...data, event: payload });
      });
  }

  function highlightInGrid(att) {
    gridApi.ensureNodeVisible(att, "middle");
    const row = gridApi.getRowNode(att.id);
    gridApi.flashCells({ rowNodes: [row] });
  }

  function createNewParty() {
    Reoverlay.showModal(NewParty, {
      onComplete({ name }) {
        Reoverlay.hideModal();
        channel.push("new_party", { name }).receive("ok", (payload) => {
          setData({ ...data, event: payload });
          setBlockData(assembleBlockData(payload));
        });
      },
    });
  }

  function deleteParty(id) {
    channel.push("delete_party", { id }).receive("ok", (payload) => {
      setData({ ...data, event: payload });
      setBlockData(assembleBlockData(payload));
    });
  }
};

createRoot(document.getElementById("grid")).render(<App />);

function assembleBlockData(event) {
  const blocks = event.blocks;
  const blockNames = blocks.map((b) => b.identifier);

  return {
    hasTitles: blockNames.includes("titles"),
    hasParties: blockNames.includes("parties"),
    hasFfxivRoles: blockNames.includes("ffxiv_roles"),
    hasDiscordLogin: blockNames.includes("discord_login"),
    ffxivRolesRolesOnly:
      blockNames.includes("ffxiv_roles") &&
      blocks.find((b) => b.identifier == "ffxiv_roles")?.data?.data?.roles_only,
    parties:
      blockNames.includes("parties") &&
      blocks.find((b) => b.identifier == "parties").data
        ? blocks.find((b) => b.identifier == "parties").data.data
        : [],
    customFields:
      blockNames.includes("custom_fields") &&
      blocks.find((b) => b.identifier == "custom_fields").data?.data,
  };
}

export function titleBadgeJsx(title, large = false) {
  const color = createColor(title.toLowerCase());
  return (
    <span
      className={`badge ${large && "fs-7"} ${
        contrast(color) == "light" ? "text-black" : "text-white"
      }`}
      style={{ backgroundColor: color }}
    >
      {title}
    </span>
  );
}
