import liveView from "./lib/liveView";

const hooks = {
  Hook: {
    mounted() {
      this.handleEvent("clearInput", (payload) => {
        const input = document.getElementById(`input-${payload.id}`);
        input.value = "";
      });
    },
  },
};

liveView(hooks);
