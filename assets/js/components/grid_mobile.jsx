import React from "react";
import day from "dayjs";
import relativeTime from "dayjs/plugin/relativeTime";
import { formatRoleJsx, sortByRole } from "../lib/roles";
import { displayCustomFieldContents } from "./grid";
import mapValues from "lodash.mapvalues";
import groupBy from "lodash.groupby";
day.extend(relativeTime);

export default function ({ data, blockData }) {
  const groups = mapValues(
    groupBy(
      data.event.attendees.filter((att) => att.custom_data.party),
      (att) => att.custom_data.party.id
    ),
    (v) => v.sort(sortByRole)
  );
  return (
    <>
      <p>
        Sorted by <strong>sign up date</strong>. This is a condensed view for
        mobile users.
      </p>
      <details>
        <summary>
          <strong>Parties</strong>
        </summary>
        {blockData.hasParties &&
          blockData.parties.map((party) => (
            <>
              {party.name}:
              <ul key={party.id}>
                {groups[party.id] && groups[party.id].map((att) => (
                  <li style={{ "margin-bottom": 5 }} key={att.id}>
                    {blockData.hasFfxivRoles &&
                      formatRoleJsx(att.custom_data.party.role)}
                    {att.ign}
                  </li>
                ))}
              </ul>
            </>
          ))}
      </details>
      <hr />
      {data.event &&
        data.event.attendees
          .sort((a, b) => day(a.inserted_at).isBefore(day(b.inserted_at)))
          .map((att) => (
            <details className="py-2" key={att.id}>
              <summary>
                {att.ign}
                {blockData.parties && att.custom_data?.party?.id && (
                  <span>
                    &nbsp;(
                    {blockData.hasFfxivRoles &&
                      formatRoleJsx(att.custom_data.party.role, 20)}
                    &nbsp;
                    {
                      blockData.parties.find(
                        (p) => p.id == att.custom_data.party.id
                      ).name
                    }
                    )
                  </span>
                )}
              </summary>
              <div className="ms-2">
                <p className="mt-2">
                  <strong>Signed up</strong>:{" "}
                  {day(att.inserted_at).format("DD/MM/YY HH:mm")} (
                  {day(att.inserted_at).fromNow()})
                </p>
                {data.store_name && (
                  <p>
                    <strong>Name</strong>: {att.name || "No name specified"}
                  </p>
                )}
                {blockData.hasFfxivRoles && (
                  <p>
                    <strong>Roles</strong>:&nbsp;
                    {att.custom_data.ffxiv_roles.split(",").map((role) => (
                      <span key={role}>{formatRoleJsx(role, 20)}</span>
                    ))}
                  </p>
                )}
                {blockData.customFields &&
                  blockData.customFields.map((field) => (
                    <p key={field.id}>
                      <strong>{field.short_name || field.name}</strong>:&nbsp;
                      {displayCustomFieldContents(
                        att.custom_data[field.id],
                        field
                      )}
                    </p>
                  ))}
              </div>
            </details>
          ))}
    </>
  );
}
