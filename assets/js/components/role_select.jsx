import React from "react";
import { ModalWrapper } from "reoverlay";
import { formatRoleJsx, expandRoles } from "../lib/roles";
import clsx from "clsx";

export default function RoleSelect(params) {
  const {
    onComplete,
    attendee,
    partyName,
    blockData,
    reassigningAtt = null,
  } = params;
  let preferredChoices = attendee.custom_data.ffxiv_roles.split(",");
  if (!blockData.ffxivRolesRolesOnly) {
    preferredChoices = expandRoles(attendee.custom_data.ffxiv_roles)
  }

  return (
    <ModalWrapper>
      <div className="container px-5 py-3">
        <h3>
          Assign a role for {attendee.ign} in {partyName}
        </h3>
        <div className="text-center">
          {blockData.ffxivRolesRolesOnly ? (
            <>
              <RoleChoice role="tank" />
              <RoleChoice role="rghealer" />
              <RoleChoice role="shhealer" />
              <RoleChoice role="melee" />
              <RoleChoice role="physranged" />
              <RoleChoice role="magicranged" />
            </>
          ) : (
            <>
              <RoleChoice role="paladin" small />
              <RoleChoice role="warrior" small />
              <RoleChoice role="darkknight" small />
              <RoleChoice role="gunbreaker" small />
              <RoleChoice role="whitemage" small />
              <RoleChoice role="scholar" small />
              <RoleChoice role="astrologian" small />
              <RoleChoice role="sage" small />
              <RoleChoice role="monk" small />
              <RoleChoice role="samurai" small />
              <RoleChoice role="dragoon" small />
              <RoleChoice role="ninja" small />
              <RoleChoice role="reaper" small />
              <RoleChoice role="bard" small />
              <RoleChoice role="machinist" small />
              <RoleChoice role="dancer" small />
              <RoleChoice role="redmage" small />
              <RoleChoice role="blackmage" small />
              <RoleChoice role="summoner" small />
            </>
          )}
        </div>
      </div>
    </ModalWrapper>
  );

  function RoleChoice({ role, small }) {
    const isExistingRole =
      reassigningAtt && reassigningAtt.custom_data.party.role === role;
    const buttonClass = clsx(["role-button"], {
      highlighted: preferredChoices.includes(role),
      "bg-black": isExistingRole,
    });
    return (
      <button className={buttonClass} onClick={() => onComplete(role)}>
        {formatRoleJsx(role, small ? 30 : 40)}
        {preferredChoices.includes(role) && (
          <div
            className={`${
              isExistingRole ? "text-white" : "text-muted"
            } d-block mt-1`}
          >
            {isExistingRole ? (
              <>
                Current
                <br />
                assignment
              </>
            ) : (
              <>
                {blockData.ffxivRolesRolesOnly ? formatPreferredChoice(preferredChoices.indexOf(role)) : "Preferred"} <br />
                Choice
              </>
            )}
          </div>
        )}
      </button>
    );
  }
}

function formatPreferredChoice(index) {
  switch (index) {
    case 0:
      return "First";
    case 1:
      return "Second";
    case 2:
      return "Third";
    case 3:
      return "Fourth";
  }
}
