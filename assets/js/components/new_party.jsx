import React, { useState } from "react";
import { ModalWrapper } from "reoverlay";

export default function NewParty({ onComplete }) {
  const [name, setName] = useState("");
  const [error, setError] = useState(null);

  function complete() {
    if (name.length === 0) {
      setError("Please assign a name to the party!");
      return;
    }

    setError(null);
    onComplete({ name });
  }

  return (
    <ModalWrapper>
      <div className="container px-5 py-3">
        <h3>New Party</h3>
        <fieldset>
          {error && (
            <div className="alert alert-danger" role="alert">
              {error}
            </div>
          )}
          <label htmlFor="partyName" className="form-label">
            Name
          </label>
          <input
            type="text"
            className="form-control"
            id="partyName"
            onChange={(evt) => setName(evt.target.value)}
            placeholder="my cool party 1"
          />
        </fieldset>
        <button className="btn btn-primary mt-2" onClick={complete}>
          Create
        </button>
      </div>
    </ModalWrapper>
  );
}
