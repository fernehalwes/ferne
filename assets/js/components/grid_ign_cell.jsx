import React, { useContext } from "react";
import { Context } from "../manage_attendees";
import clsx from "clsx";
import Tippy from "@tippyjs/react";

export default function GridIgnCell({ context, data, ...props }) {
  const { listGroups } = useContext(Context);
  const { event } = context;
  const isOnWorry =
    listGroups.worry &&
    listGroups.worry.length > 0 &&
    listGroups.worry.find(
      (l) => l.ign.toLowerCase() === data.ign.toLowerCase()
    );
  const isOnFavor =
    listGroups.favor &&
    listGroups.favor.length > 0 &&
    listGroups.favor.find(
      (l) => l.ign.toLowerCase() === data.ign.toLowerCase()
    );
  const isTeam =
    event.team_members &&
    event.team_members.find(
      (m) => m.name.toLowerCase() === data.ign.toLowerCase()
    );
  const classes = clsx({
    "text-danger": isOnWorry,
    "text-primary": isTeam,
    "text-success": isOnFavor,
  });

  return (
    <>
      {context.edit_permission && renderButtons(props)}
      <strong className={classes}>{data.ign}</strong>
    </>
  );
}

function clickHandler(props, type) {
  return function () {
    props[`${type}Clicked`](props.node.data);
  };
}

function renderButtons(props) {
  return (
    <>
      <Tippy content="Edit attendee data">
        <span
          onClick={clickHandler(props, "edit")}
          role="button"
          className="d-inline-block me-1"
        >
          <i className="bi bi-pencil" width={10}></i>
        </span>
      </Tippy>
      <Tippy content="Delete attendee">
        <span
          onClick={clickHandler(props, "delete")}
          role="button"
          className="d-inline-block me-1"
        >
          <i className="bi bi-trash" width={10}></i>
        </span>
      </Tippy>
    </>
  );
}
