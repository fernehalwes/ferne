import React, { useContext } from "react";
import { Context } from "../manage_attendees";
import Attendee from "./attendee";
import { sortByRole } from "../lib/roles";

export default function Party({ party, data, dropState, setDropState }) {
  const { reassigning, reassigningAtt, assignToPartyWithRole, deleteParty } =
    useContext(Context);
  const partyAttendees = data.event.attendees.filter(
    (a) => a.custom_data.party && a.custom_data.party.id == party.id
  );
  const isSameParty =
    reassigningAtt && reassigningAtt.custom_data.party.id === party.id;
  return (
    <div
      key={party.id}
      className={`card mb-2 ${reassigning && "party-highlighted-hover col-6"} ${
        dropState[party.id] && "party-highlighted"
      } ${isSameParty && "party-highlighted"}`}
      id={`party-${party.id}`}
    >
      {reassigning ? (
        <div className="card-body" onClickCapture={doReassign}>
          <h5 className="align-middle text-center mb-0">
            <i className="bi bi-plus"></i>
            {isSameParty ? (
              "Currently in "
            ) : (
              <>{reassigningAtt ? "Click" : "Drop"} to add to </>
            )}
            <span className="text-primary">{party.name}</span>
          </h5>
          <div className="text-muted text-center">
            {partyAttendees.length} members
          </div>
        </div>
      ) : (
        <div className="card-body">
          <h4 className="card-title">
            {party.name}
            <div className="float-end">
              {data.edit_permission && (
                <button
                  className="btn btn-sm btn-outline-danger"
                  onClick={() => deleteParty(party.id)}
                >
                  <i className="bi bi-trash"></i>
                </button>
              )}
            </div>
          </h4>
          <div className="list-group list-group-flush party">
            {partyAttendees.sort(sortByRole).map((att) => (
              <Attendee
                key={att.id}
                attendee={att}
                edit_permission={data.edit_permission}
              />
            ))}
          </div>
        </div>
      )}
    </div>
  );

  function doReassign(evt) {
    if (reassigning && reassigningAtt) {
      assignToPartyWithRole(reassigningAtt, party);
      setDropState({ [party.id]: false, ...dropState });
    }
  }
}
