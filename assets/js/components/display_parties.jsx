import chunk from "lodash.chunk";
import groupBy from "lodash.groupby";
import mapValues from "lodash.mapvalues";
import React, { useState } from "react";
import { ModalWrapper } from "reoverlay";
import { sortByRole } from "../lib/roles";
import { titleBadgeJsx } from "../manage_attendees";

export default function DisplayParties({ data, blockData }) {
  const [showIcons, setShowIcons] = useState(true);
  const groups = mapValues(
    groupBy(
      data.event.attendees.filter((att) => att.custom_data.party),
      (att) => att.custom_data.party.id
    ),
    (v) => v.sort(sortByRole)
  );
  const chunked = chunk(blockData.parties, 7);
  const lengths = Object.values(groups).map((g) => g.length);
  const longestLength = Math.max(...lengths);

  return (
    <ModalWrapper contentContainerClassName="p-4">
      <h2>Display Parties</h2>

      {blockData.hasFfxivRoles && (
        <fieldset className="form-check">
          <input
            type="checkbox"
            className="form-check-input"
            id="showIcons"
            checked={showIcons}
            onChange={() => setShowIcons(!showIcons)}
          />
          <label for="showIcons" className="form-check-label align-middle">
            Show role icons?
          </label>
        </fieldset>
      )}

      {(groups.length === 0 || longestLength === 0) && "Nothing here yet!"}
      {chunked.map((parties) => (
        <div class="table-responsive">
          <table className="table table-sm mb-0 party-display">
            <thead>
              <tr className="bg-white">
                {parties.map((party) => (
                  <th className="fs-5">{party.name}</th>
                ))}
              </tr>
            </thead>
            <tbody>
              {Array(longestLength)
                .fill()
                .map((_, i) => (
                  <tr>
                    {parties.map((party) => (
                      <td
                        className={`${
                          groups[party.id]
                            ? getRoleCssIfExists(groups[party.id][i])
                            : "role-none"
                        }`}
                      >
                        {groups[party.id] && groups[party.id][i] && (
                          <div className="d-flex justify-content-start align-items-center">
                            {blockData.hasFfxivRoles && showIcons && (
                              <img
                                src={`/images/classes/${
                                  groups[party.id][i].custom_data.party.role
                                }.png`}
                                className="align-middle me-1"
                                width={20}
                              />
                            )}
                            <div
                              className={`${
                                groups[party.id][i].custom_data.party.leader &&
                                "fw-bold align-middle"
                              } fs-7`}
                            >
                              {groups[party.id][i].ign}
                              {groups[party.id][i].custom_data.title && (
                                <div>
                                  {titleBadgeJsx(
                                    groups[party.id][i].custom_data.title
                                  )}
                                </div>
                              )}
                            </div>
                          </div>
                        )}
                      </td>
                    ))}
                  </tr>
                ))}
            </tbody>
          </table>
        </div>
      ))}
    </ModalWrapper>
  );
}

function getRoleCssIfExists(attendee) {
  if (
    attendee &&
    attendee.custom_data &&
    attendee.custom_data.party &&
    attendee.custom_data.party.role
  ) {
    return `role-${attendee.custom_data.party.role}`;
  }
  if (attendee && attendee.custom_data && attendee.custom_data.party) {
    return "role-none";
  }
  return "";
}
