import React, { useContext } from "react";
import { Context, titleBadgeJsx } from "../manage_attendees";
import { formatRoleJsx } from "../lib/roles";
import Tippy from "@tippyjs/react";

export default function Attendee({ attendee, edit_permission }) {
  const {
    setReassigning,
    setReassigningAtt,
    highlightInGrid,
    removeAssignment,
    toggleLeaderState,
  } = useContext(Context);
  return (
    <div
      key={attendee.id}
      className={`list-group-item member show-container d-flex align-items-center ${
        attendee.custom_data.party.role
          ? `role-${attendee.custom_data.party.role}`
          : "role-none"
      }`}
    >
      {attendee.custom_data.party.role && (
        <span className="align-middle">
          {formatRoleJsx(attendee.custom_data.party.role)}
        </span>
      )}
      <span className="ms-2 fs-7">
        <span className={attendee.custom_data.party.leader && "fw-bold"}>
          {attendee.ign}
        </span>
        {attendee.custom_data.title && (
          <span className="d-inline-block ms-1">
            {titleBadgeJsx(attendee.custom_data.title)}
          </span>
        )}
      </span>
      {edit_permission && (
        <span className="inline-block ms-auto">
          <Tippy
            content={`${
              attendee.custom_data.party.leader ? "Remove" : "Give"
            } leader`}
          >
            <span
              role="button"
              style={{ fontSize: 20 }}
              className="show-hover mx-1"
              onClick={() => {
                toggleLeaderState(attendee);
              }}
            >
              {attendee.custom_data.party.leader ? (
                <i className="bi bi-shield-fill-x"></i>
              ) : (
                <i className="bi bi-shield-fill"></i>
              )}
            </span>
          </Tippy>
          <Tippy content="Highlight in attendee list">
            <span
              role="button"
              style={{ fontSize: 20 }}
              className="show-hover mx-1"
              onClick={() => {
                highlightInGrid(attendee);
              }}
            >
              <i className="bi bi-search"></i>
            </span>
          </Tippy>
          <Tippy content="Reassign party">
            <span
              role="button"
              style={{ fontSize: 20 }}
              className="show-hover mx-1"
              onClick={() => {
                setReassigning(true);
                setReassigningAtt(attendee);
              }}
            >
              <i className="bi bi-arrow-repeat"></i>
            </span>
          </Tippy>
          <Tippy content="Remove assignment">
            <span
              role="button"
              style={{ fontSize: 20 }}
              className="show-hover mx-1"
              onClick={() => {
                removeAssignment(attendee);
              }}
            >
              <i className="bi bi-person-x"></i>
            </span>
          </Tippy>
        </span>
      )}
    </div>
  );
}
