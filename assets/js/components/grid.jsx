import React, { useState, useEffect, useContext, useRef } from "react";
import { AgGridReact, AgGridColumn } from "ag-grid-react";
import day from "dayjs";
import relativeTime from "dayjs/plugin/relativeTime";
import { formatRoles, formatRole, formatRoleJsx } from "../lib/roles";
import { Context, titleBadgeJsx } from "../manage_attendees";
import GridIgnCell from "./grid_ign_cell";

export default function Grid(props) {
  day.extend(relativeTime);
  const {
    data,
    setData,
    blockData,
    setGridApi,
    // setColumnApi,
    channel,
    setSelectedAttendees,
  } = props;
  const { setReassigning, listGroups } = useContext(Context);
  const [colDefs, setColDefs] = useState([]);
  const [hasLoaded, setHasLoaded] = useState(false);
  const [hiddenCols, setHiddenCols] = useState([]);
  const gridRef = useRef();
  useEffect(() => {
    if (!hasLoaded && data.event.name) {
      const colDefs = [
        {
          field: "ign",
          headerName: "IGN",
          pinned: "left",
          width: 200,
          cellRenderer: GridIgnCell,
          cellRendererParams: {
            editClicked: (data) => {
              channel
                .push("edit", {
                  aid: data.id,
                })
                .receive("ok", (url) => {
                  window.location.href = url;
                });
            },
            deleteClicked: (data) => {
              if (window.confirm(`Really delete ${data.ign}?`)) {
                channel
                  .push("delete", { aid: data.id })
                  .receive("ok", (payload) => {
                    setData({ ...data, event: payload });
                  });
              }
            },
          },
          tooltipValueGetter: (params) => {
            if (!data.lists) return null;
            const match = data.lists.find(
              (d) => d.ign.toLowerCase() === params.value.toLowerCase()
            );
            if (match) {
              return `${match.type} list: ${match.reason || "no reason given"}`;
            }
            return null;
          },
        },
      ];
      if (data.event.store_name) {
        colDefs.push({ field: "name", headerName: "Name" });
      }
      if (blockData.hasTitles) {
        colDefs.push({
          colId: "title",
          headerName: "Title",
          valueGetter: (params) => params.data.custom_data.title,
          valueSetter: (params) => {
            channel
              .push("set_custom_data_field", {
                uid: params.data.id,
                field: "title",
                value: params.newValue,
              })
              .receive("ok", (payload) => setData({ ...data, event: payload }));
          },
          cellRenderer: (params) => titleBadgeJsx(params.value || "", true),
          editable: data.edit_permission,
        });
      }
      if (blockData.hasParties) {
        colDefs.push({
          colId: "party",
          headerName: "Party",
          valueGetter: partyGetter(blockData),
          cellRenderer: formatParties(blockData),
        });
      }
      if (blockData.hasFfxivRoles) {
        colDefs.push({
          colId: "roles",
          headerName: "Roles",
          valueGetter: (params) => params.data.custom_data.ffxiv_roles,
          cellRenderer: (params) => formatRoles(params.value),
        });
      }
      if (blockData.hasDiscordLogin) {
        colDefs.push({
          colId: "discord_login",
          headerName: "Discord",
          editable: true,
          valueGetter: (params) => {
            if (params.data.custom_data.discord_login) {
              return params.data.custom_data.discord_login.username;
            }
            return params.data.custom_data.discord_username;
          },
        });
      }
      if (blockData.customFields) {
        blockData.customFields.forEach((field) => {
          colDefs.push({
            colId: field.id,
            headerName: field.short_name,
            valueGetter: (params) =>
              displayCustomFieldContents(
                params.data.custom_data[field.id],
                field
              ),
            tooltipValueGetter: (params) =>
              displayCustomFieldContents(
                params.data.custom_data[field.id],
                field
              ),
            editable: !field.has_form,
          });
        });
      }
      colDefs.push({
        colId: "inserted_at",
        headerName: "Signed up at",
        field: "inserted_at",
        width: 200,
        autoHeight: true,
        sort: "desc",
        tooltipField: "inserted_at",
        cellRenderer: (params) => day(params.value).fromNow(),
      });
      setColDefs(colDefs);
      setHasLoaded(true);
      if (data.event.attendees.length == 0) {
        gridRef.current.api.showNoRowsOverlay();
      }
    }
  }, [data]);

  useEffect(() => {
    if (data.event.name) {
      const colState = window.localStorage.getItem(
        `ferne:manage-${window.eventId}`
      );
      if (colState) {
        gridRef.current.columnApi.applyColumnState({
          state: JSON.parse(colState),
          applyOrder: true,
        });
      }
    }
  }, [colDefs]);

  return (
    <>
      <AgGridReact
        ref={gridRef}
        context={data}
        rowData={data.event.attendees}
        columnDefs={colDefs}
        getRowId={({ data }) => data.id}
        tooltipShowDelay={200}
        rowSelection={"multiple"}
        defaultColDef={{
          resizable: true,
          sortable: true,
          filter: true,
          floatingFilter: true,
          width: 150,
        }}
        animateRows={true}
        getRowClass={(params) => {
          if (!!params.data.selected) {
            return "confirmed";
          }
        }}
        suppressScrollOnNewData={true}
        onRowDragEnter={() => setReassigning(true)}
        onRowDragEnd={() => setReassigning(false)}
        onGridReady={(params) => {
          setGridApi(params.api);
          params.api.showLoadingOverlay();
        }}
        onColumnMoved={saveColumnState}
        onColumnPinned={saveColumnState}
        onColumnVisible={(evt) => {
          const allHiddenCols = evt.columnApi
            .getAllColumns()
            .filter((col) => !col.visible);
          saveColumnState(evt);
          setHiddenCols(allHiddenCols);
        }}
        onColumnResized={saveColumnState}
        onFilterChanged={saveColumnState}
        onSelectionChanged={updateSelected}
        rowDragEntireRow={data.edit_permission}
        suppressReactUi={true}
      ></AgGridReact>
      {hiddenCols.length > 0 && (
        <div className="d-flex mt-1">
          <div>Unhide column:</div>
          {hiddenCols.map((col) => (
            <div className="mx-1">
              <span
                className="text-primary cursor-pointer text-decoration-underline"
                onClick={() => unhideColumn(col)}
              >
                {col.userProvidedColDef.headerName}
              </span>
            </div>
          ))}
        </div>
      )}
    </>
  );

  function updateSelected(event) {
    setSelectedAttendees(event.api.getSelectedNodes().map((node) => node.data));
  }

  function unhideColumn(col) {
    gridRef.current.columnApi.setColumnVisible(col.colId, true);
  }
}

function saveColumnState(event) {
  window.localStorage.setItem(
    `ferne:manage-${window.eventId}`,
    JSON.stringify(event.columnApi.getColumnState())
  );
}

const partyGetter = (blockData) => (params) => {
  if (!blockData.parties) {
    return null;
  }
  const party = blockData.parties.find(
    (p) =>
      p.id ===
      (params.data.custom_data.party && params.data.custom_data.party.id)
  );
  if (blockData.hasFfxivRoles) {
    return party && `${params.data.custom_data.party.role} ${party.name}`;
  }
  return party && `${party.name}`;
};

const formatParties = (blockData) => (params) => {
  if (params.value) {
    const split = params.value.split(" ");
    if (blockData.hasFfxivRoles) {
      return (
        <span>
          {formatRoleJsx(split[0])} {split.slice(1).join(" ")}
        </span>
      );
    }
    return split.join(" ");
  }
  return "";
};

export function displayCustomFieldContents(content, field) {
  if (field.type === "boolean") {
    return content ? "Yes" : "No";
  }
  if (field.type === "single_select") {
    const match = field.choices.find(({ id }) => id === content);
    return match.value;
  }
  if (field.type === "multi_select") {
    if (Array.isArray(content)) {
      const matches = field.choices.filter(({ id }) => content.includes(id));
      return matches.map((match) => match.value).join(", ");
    }
    return null;
  }
  return content;
}

const renderIgnCell = (listGroups, event) => (params) => {
  if (
    listGroups.worry &&
    listGroups.worry.length > 0 &&
    listGroups.worry.find(
      (l) => l.ign.toLowerCase() === params.value.toLowerCase()
    )
  ) {
    return `<strong class="text-danger">${params.value}</strong>`;
  }
  if (
    listGroups.favor &&
    listGroups.favor.length > 0 &&
    listGroups.favor.find(
      (l) => l.ign.toLowerCase() === params.value.toLowerCase()
    )
  ) {
    return `<strong class="text-success">${params.value}</strong>`;
  }
  if (
    event.team_members &&
    event.team_members.find(
      (m) => m.name.toLowerCase() === params.value.toLowerCase()
    )
  ) {
    return `<strong class="text-primary">${params.value}</strong>`;
  }
  return `<strong>${params.value}</strong>`;
};
