import React, { useEffect, useState, useContext } from "react";
import { Context } from "../manage_attendees";
import Party from "./party";

export default function Parties({ data, blockData, gridApi }) {
  const { reassigning, setReassigning, reassigningAtt, assignToPartyWithRole } =
    useContext(Context);
  if (!blockData.hasParties) {
    return <p>This event is not configured to have parties.</p>;
  }

  const [dropState, setDropState] = useState(
    blockData.parties.reduce((acc, p) => (acc[p] = false), {})
  );

  useEffect(() => {
    blockData.parties.forEach((party) => {
      gridApi.addRowDropZone({
        getContainer() {
          return document.getElementById(`party-${party.id}`);
        },
        onDragEnter() {
          setDropState({ [party.id]: true, ...dropState });
        },
        onDragStop(params) {
          setReassigning(false);
          setDropState({ [party.id]: false, ...dropState });
          assignToPartyWithRole(params.node.data, party);
        },
      });
    });
    return () => {
      blockData.parties.forEach((party) => {
        gridApi.removeRowDropZone({
          getContainer() {
            return document.getElementById(`party-${party.id}`);
          },
        });
      });
    };
  }, [blockData.parties]);

  return (
    <div className={reassigning && "row"}>
      {reassigning && reassigningAtt && (
        <h4 className="text-center">Reassign {reassigningAtt.ign}</h4>
      )}
      {blockData.parties.map((party) => (
        <Party
          key={party.id}
          party={party}
          data={data}
          dropState={dropState}
          setDropState={setDropState}
        />
      ))}
    </div>
  );
}
