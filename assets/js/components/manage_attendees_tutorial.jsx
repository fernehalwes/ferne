import React from "react";
import { ModalWrapper, Reoverlay } from "reoverlay";

export default function Tutorial() {
  return (
    <ModalWrapper
      onClose={() => {
        localStorage.setItem("ferne:manage_tutorial", true);
        Reoverlay.hideModal();
      }}
    >
      <div className="fs-6 container p-4" style={{ maxWidth: 650 }}>
        <p>
          <b>Hello!</b> Welcome to Ferne's attendee management page. Before you
          get started, here's a couple of pointers, since there's a lot of
          functionality.
        </p>
        <p>
          Try playing around with the following:
          <ul>
            <li>Reordering columns by dragging their headers</li>
            <li>
              Resizing columns like you would in other sheets applications
            </li>
            <li>Selecting multiple attendees by holding Control/Command</li>
            <li>Adding a new party!</li>
            <li>
              Adding an attendee to a party by dragging them to the parties
              sidebar
            </li>
            <li>
              Hiding columns you don't want to see by dragging them outside of
              the grid
            </li>
          </ul>
        </p>
        <p>
          All adjustments you make to the grid layout are saved between reloads,
          per event. Have fun organizing!
        </p>
        <p className="text-muted mb-0">This message will only display once.</p>
      </div>
    </ModalWrapper>
  );
}
