import flatpickr from "flatpickr";
import liveView from "./lib/liveView";

let hooks = {
  RomeMount: {
    mounted() {
      flatpickr(this.el, {
        enableTime: true,
        dateFormat: "Y-m-d H:i",
        time_24hr: true,
        allowInput: true,
        defaultDate: this.el.dataset["value"],
      });
    },
  },
};

liveView(hooks);
