import flatpickr from "flatpickr";

flatpickr(document.getElementById("time-mount"), {
  enableTime: true,
  dateFormat: "Y-m-d H:i",
  time_24hr: true,
  allowInput: true,
});
