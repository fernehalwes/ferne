// We import the CSS which is extracted to its own file by esbuild.
import "../css/app.css";
import "flatpickr/dist/flatpickr.min.css";

import "phoenix_html";
import Popper from "@popperjs/core";
window.Popper = Popper;
import tippy from "tippy.js";
import "tippy.js/dist/tippy.css";
window.tippy = tippy;
window.globalTippies = tippy("[data-tippy-content]");
import "bootstrap/js/dist/collapse";
import "bootstrap/js/dist/dropdown";
import "bootstrap/js/dist/modal";
import "bootstrap/js/dist/tab";

import day from "dayjs";
import utc from "dayjs/plugin/utc";
day.extend(utc);
const stMount = document.getElementById("stMount");
let dotOn = true;
stMount.innerHTML = getServerTime(":");
setInterval(() => {
  stMount.innerHTML = getServerTime(dotOn ? " " : ":");
  dotOn = !dotOn;
}, 1500); // 1.5 secs

function getServerTime(sep) {
  return day()
    .utc()
    .format(`HH<co[d]e [class="text-white"]>${sep}</co[d]e>mm ST`);
}
