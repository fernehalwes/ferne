# Fernehalwes

Fernehalwes (Ferne for short) is a website that helps people host events in FFXIV.

## Local Setup

Once you've cloned this repository, you want to make sure you've got
[Elixir](https://elixir-lang.org) installed. You also need a PostgreSQL version 11+
database server running somewhere. For the frontend, you
need Node.js, as well. Preferably a recent version.

The first step is to install dependencies, using `mix deps.get`. This may
prompt you to install something called Hex, that's okay. Then, install the local
dependencies for the frontend using `npm install` or `npm ci` (that's faster).

Check the `config/dev.exs` file to set your local database credentials. By default,
it uses socket auth, meaning that it relies on the default PostgreSQL config that local
clients get passwordless access. If that's different for you, add a `password` key. Make sure
to never check in these changes, since they're local to you.

Now you can set up your database! There's a shortcut task for this: `mix ecto.setup`. This
creates and runs migrations on your database.

You should be ready for running Fernehalwes locally now. There's two main commands to do this:

- `mix phx.server`: This is your standard "run the app" command.
- `iex -S mix phx.server`: This _does_ run the previous command, but it also gives you a neat
  interactive console (think `rails console`) so you can mess around with stuff. This is what
  I personally use, and I'd recommend it.

Fernehalwes is available locally on https://localhost:4000.
Both commands automatically (re)compile assets. In development, no email provider is set up,
so you can use the local mailbox at https://localhost:4000/dev/mailbox to catch emails sent to
any address.
