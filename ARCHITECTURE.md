# Architecture

This document describes the high-level architecture of Ferne, along with some specifics
related to how we use Elixir and Phoenix conventions.

## Bird's Eye View

Ferne is a standard [Phoenix](https://phoenixframework.org) application that
uses a PostgreSQL database as a data store. It's deployed to [Fly](https://fly.io)
using a Dockerfile. Some dynamic parts of the frontend are implemented using
Phoenix LiveView, some more complex ones use [Preact](https://preactjs.com).

## Conventions

### Elixir

Elixir is linted with `mix format` and `credo`.

### Phoenix

_(Note: Some of this stuff is in the process of being refactored.)_

We don't use context modules. Most of the model-related business logic that has to do with
data CRUD actions is located in the model module. Specifically, the following:

- Schema
- Changesets
- Query blocks (e.g. `functionname(query \\ __MODULE__) :: query`)
- Other CRUD actions (inserts, updates, deletes, etc)

Functions should be in that order. Changeset functions end with `_changeset`, query
functions either are the schema field itself or something like `by_{attribute}`, and CRUD
functions are `get_{whatever}`, `insert_{whatever}`, and so on.

Model modules inherit from a shared Schema module that provides some common functionality
(sorting, pagination and so forth).

### JS

We use [esbuild](https://esbuild.github.io) for building. Preact is used, as stated before,
with hooks and functional components. Linting is done via Prettier.
