defmodule Ferne.User do
  @moduledoc """
  Schema module for users.
  """
  use Ferne.Schema
  import Ecto.Changeset
  import Ecto.Query
  alias Ferne.{Repo, UserToken}
  @primary_key {:id, :binary_id, autogenerate: true}
  @foreign_key_type :binary_id
  @derive {Jason.Encoder,
           only: [
             :name,
             :username
           ]}
  schema "users" do
    field :email, :string
    field :name, :string
    field :username, :string
    field :confirmed_at, :naive_datetime

    has_many :events, Ferne.Event, on_delete: :delete_all
    has_many :teams, Ferne.Team

    many_to_many :organized_events, Ferne.Event,
      join_through: Ferne.EventOrganizer,
      on_delete: :nothing

    many_to_many :team_memberships, Ferne.Team, join_through: "team_members"

    timestamps()
  end

  @doc """
  A user changeset for registration.
  """
  def registration_changeset(user, attrs) do
    user
    |> cast(attrs, [:email, :name, :username])
    |> validate_username()
    |> validate_email()
    |> validate_name()
  end

  defp validate_username(changeset) do
    changeset
    |> validate_required([:username])
    |> validate_length(:username, max: 35)
    |> validate_format(:username, ~r/^[_A-Za-z0-9]*$/,
      message: "invalid format for username! use only a-z, 0-9 and _"
    )
    |> unsafe_validate_unique(:username, Ferne.Repo)
    |> unique_constraint(:username)
  end

  defp validate_email(changeset) do
    changeset
    |> validate_required([:email])
    |> validate_format(:email, ~r/^[^\s]+@[^\s]+$/, message: "must have the @ sign and no spaces")
    |> validate_length(:email, max: 160)
    |> unsafe_validate_unique(:email, Ferne.Repo)
    |> unique_constraint(:email)
  end

  defp validate_name(changeset) do
    changeset
    |> validate_length(:name, max: 50)
  end

  @doc """
  A user changeset for changing the profile settings.
  """
  def profile_changeset(user, attrs) do
    user
    |> cast(attrs, [:name, :username])
    |> validate_username()
    |> validate_name()
  end

  @doc """
  A user changeset for changing the email.

  It requires the email to change otherwise an error is added.
  """
  def email_changeset(user, attrs) do
    user
    |> cast(attrs, [:email])
    |> validate_email()
    |> case do
      %{changes: %{email: _}} = changeset -> changeset
      %{} = changeset -> add_error(changeset, :email, "did not change")
    end
  end

  def by_like_username(query \\ __MODULE__, name),
    do: from(q in query, where: ilike(q.username, ^name))

  defp email_multi(user, email, context) do
    changeset = user |> email_changeset(%{email: email}) |> confirm_changeset()

    Ecto.Multi.new()
    |> Ecto.Multi.update(:user, changeset)
    |> Ecto.Multi.delete_all(:tokens, UserToken.user_and_contexts_query(user, [context]))
  end

  @doc "Confirms the account by setting `confirmed_at`."
  def confirm_changeset(user) do
    now = NaiveDateTime.utc_now() |> NaiveDateTime.truncate(:second)
    change(user, confirmed_at: now)
  end

  @doc "Gets a user by email."
  def get_by_email(email) when is_binary(email) do
    Repo.get_by(__MODULE__, email: email)
  end

  @doc "Finds all users that match a name."
  def search_by_username(name) do
    str = "%#{name}%"

    by_like_username(str)
    |> Repo.all()
  end

  @doc "Registers a user."
  def register(attrs) do
    %__MODULE__{}
    |> registration_changeset(attrs)
    |> Repo.insert()
  end

  @doc "Updates a user's profile."
  def update_profile(user, attrs) do
    changeset = profile_changeset(user, attrs)

    Repo.update(changeset)
  end

  @doc """
  Emulates that the email will change without actually changing
  it in the database.
  """
  def apply_email(user, attrs) do
    user
    |> email_changeset(attrs)
    |> Ecto.Changeset.apply_action(:update)
  end

  @doc """
  Updates the user email using the given token.

  If the token matches, the user email is updated and the token is deleted.
  The confirmed_at date is also updated to the current time.
  """
  def update_email(user, token) do
    context = "change:#{user.email}"

    with {:ok, query} <- UserToken.verify_change_email_token_query(token, context),
         %UserToken{sent_to: email} <- Repo.one(query),
         {:ok, _} <- Repo.transaction(email_multi(user, email, context)) do
      :ok
    else
      _ -> :error
    end
  end
end
