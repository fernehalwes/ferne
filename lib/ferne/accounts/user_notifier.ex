defmodule Ferne.UserNotifier do
  @moduledoc """
  This delivers email.
  """
  import Swoosh.Email

  alias Ferne.Mailer

  # Delivers the email using the application mailer.
  defp deliver(recipient, subject, body) do
    email =
      new()
      |> to(recipient)
      |> from({"Fernehalwes", "noreply@fernehalwes.org"})
      |> subject(subject)
      |> text_body(body)

    with {:ok, _metadata} <- Mailer.deliver(email) do
      {:ok, email}
    end
  end

  @doc """
  Deliver instructions to confirm account.
  """
  def deliver_confirmation_instructions(user, url) do
    deliver(user.email, "Confirmation instructions", """

    ==============================

    Hi #{user.email},

    You can confirm your account by visiting the URL below:

    #{url}

    If you didn't create an account with us, please ignore this.

    ==============================
    """)
  end

  @doc """
  Deliver a magic login link to log the user in.
  """
  def deliver_login_link(user, url) do
    deliver(user.email, "Log in to Fernehalwes", """

    ==============================

    Hi #{user.email},

    you have requested to log into Fernehalwes. Click this
    URL to log in. Keep in mind that this link is only valid
    for 10 minutes.

    #{url}

    If you didn't request to log in, ignore this (and check whether
    your email account has been compromised).

    ==============================
    """)
  end

  @doc """
  Deliver instructions to reset a user password.
  """
  def deliver_reset_password_instructions(user, url) do
    deliver(user.email, "Reset password instructions", """

    ==============================

    Hi #{user.email},

    You can reset your password by visiting the URL below:

    #{url}

    If you didn't request this change, please ignore this.

    ==============================
    """)
  end

  @doc """
  Deliver instructions to update a user email.
  """
  def deliver_update_email_instructions(user, url) do
    deliver(user.email, "Update email instructions", """

    ==============================

    Hi #{user.email},

    You can change your email by visiting the URL below:

    #{url}

    If you didn't request this change, please ignore this.

    ==============================
    """)
  end

  @doc """
  Deliver instructions to join a team.
  """
  def deliver_team_invite(user, team, url) do
    deliver(user.email, "You've been invited to join #{team.name} on Fernehalwes", """

    ==============================

    Hi #{user.email},

    You have been invited to join the team #{team.name} on Fernehalwes.
    A team is useful to plan re-occurring events together. If you're not
    sure why you've received this email, you can safely ignore it.

    To join the team, visit the URL below:

    #{url}

    ==============================
    """)
  end
end
