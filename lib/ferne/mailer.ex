defmodule Ferne.Mailer do
  @moduledoc """
  Mailer module.
  """
  use Swoosh.Mailer, otp_app: :ferne
end
