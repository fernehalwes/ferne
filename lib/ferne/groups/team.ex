defmodule Ferne.Team do
  @moduledoc """
  Schema module for teams.
  """
  use Ferne.Schema
  import Ecto.Changeset
  import Ecto.Query
  alias Ferne.{Repo, TeamMember}

  @primary_key {:id, :binary_id, autogenerate: true}
  @foreign_key_type :binary_id
  schema "teams" do
    field :name, :string
    field :description, :string
    field :link, :string
    field :public_members, :boolean

    belongs_to :user, Ferne.User
    has_many :team_members, Ferne.TeamMember, on_delete: :delete_all
    many_to_many :members, Ferne.User, join_through: Ferne.TeamMember
    has_many :events, Ferne.Event, on_delete: :delete_all
    has_many :invites, Ferne.TeamInvite, on_delete: :delete_all
    has_many :templates, Ferne.EventTemplate, on_delete: :delete_all
    has_many :attention_lists, Ferne.AttentionList, on_delete: :delete_all

    timestamps(type: :utc_datetime)
  end

  # Only the team creator can delete the team
  def authorize(:delete_team, %{id: user_id} = _user, %{user_id: user_id} = _team), do: :ok
  def authorize(:delete_team, _user, _team), do: :error

  # Only the team creator or admin can manage the team
  def authorize(:manage_team, %{id: user_id} = _user, %{user_id: user_id} = _team), do: :ok

  def authorize(:manage_team, %{id: user_id} = _user, %{team_members: tms = _team}) do
    entry = Enum.find(tms, &(&1.user_id == user_id))
    if entry && entry.superadmin, do: :ok, else: :error
  end

  def authorize(:manage_team, _user, _team), do: :error
  # Team admins can administrate the team
  def authorize(:administrate_team, %{id: user_id} = _user, %{user_id: user_id} = _team), do: :ok

  def authorize(:administrate_team, %{id: user_id} = _user, %{team_members: members} = _team) do
    found = Enum.find(members, &(&1.user_id == user_id))
    if found && (found.admin || found.superadmin), do: :ok, else: :error
  end

  def authorize(:administrate_team, _user, _team), do: :error
  # Only team members can access the team
  def authorize(:access_team, %{id: user_id} = _user, %{members: members} = _team) do
    if(Enum.find(members, &(&1.id == user_id)), do: :ok, else: :error)
  end

  def authorize(:access_team, _user, _team), do: :error

  def creation_changeset(team, params \\ %{}) do
    team
    |> cast(params, [:name, :description, :link, :public_members])
    |> validate_required([:name, :public_members])
  end

  @doc "Filters a team query by whether the user is a member of the team."
  def by_member(query \\ __MODULE__, user),
    do: from(t in query, select: t, inner_join: m in assoc(t, :members), on: m.id == ^user.id)

  @doc """
  Get a team by its ID.
  """
  def get_by_id(id) do
    case Ecto.UUID.dump(id) do
      {:ok, _} ->
        super(id)

      :error ->
        nil
    end
  end

  @doc """
  Get all teams that a user is part of.
  """
  def get_all_for_user(user) do
    by_member(user)
    |> Repo.all()
  end

  @doc """
  Get all teams that a user is admin or creator of.
  """
  def get_all_for_user_with_admin(user) do
    query =
      from(t in __MODULE__,
        select: t,
        left_join: m in assoc(t, :team_members),
        on: m.user_id == ^user.id,
        where: t.user_id == ^user.id or m.admin or m.superadmin
      )

    Repo.all(query)
  end

  @doc """
  Creates a new team.
  """
  def create(attrs, user) do
    team_changeset =
      %__MODULE__{}
      |> creation_changeset(attrs)
      |> Ecto.Changeset.change(%{user_id: user.id})

    with {:ok, team} <- Repo.insert(team_changeset),
         member_changeset <-
           TeamMember.creation_changeset(%TeamMember{}, %{
             team_id: team.id,
             user_id: user.id,
             admin: true
           }),
         {:ok, _} <- Repo.insert(member_changeset) do
      {:ok, team}
    else
      {:error, err} -> {:error, err}
    end
  end

  @doc """
  Updates a team.
  """
  def update(team, attrs) do
    team
    |> creation_changeset(attrs)
    |> Repo.update()
  end
end
