defmodule Ferne.TeamMember do
  @moduledoc """
  Join schema between teams and users.
  """
  use Ferne.Schema
  import Ecto.Changeset
  import Ecto.Query
  alias Ferne.Repo
  @primary_key {:id, :binary_id, autogenerate: true}
  @foreign_key_type :binary_id

  schema "team_members" do
    field :user_id, :binary_id
    field :team_id, :binary_id
    # This is a bit confusing, originally, Ferne only had a "team admin" elevated
    # privilege level. Later on, we added a separate one, renamed "team admin" to
    # "team organizer" and called the new one "team admin". As far as the database
    # is concerned, though, the organizer is called admin, and the admin is called
    # "super admin".
    field :admin, :boolean, default: false
    field :superadmin, :boolean, default: false

    timestamps()
  end

  def creation_changeset(tm, attrs) do
    tm
    |> cast(attrs, [:team_id, :user_id, :admin])
    |> validate_required([:team_id, :user_id])
  end

  def edit_admin_changeset(tm, attrs) do
    tm
    |> cast(attrs, [:admin])
  end

  def edit_superadmin_changeset(tm, attrs) do
    tm
    |> cast(attrs, [:superadmin])
  end

  @doc """
  Gets the join schema for a specific team and user ID.
  """
  def get_for_team_and_user(team, user) do
    Repo.one(from(__MODULE__, where: [user_id: ^user.id, team_id: ^team.id]))
  end
end
