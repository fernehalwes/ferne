defmodule Ferne.TeamInvite do
  @moduledoc """
  Schema module for a team invitation.
  """
  use Ferne.Schema
  import Ecto.Query
  alias Ferne.{Repo, Team}

  @hash_algorithm :blake2b
  @rand_size 32
  @validity_in_days 1

  @primary_key {:id, :binary_id, autogenerate: true}
  @foreign_key_type :binary_id
  schema "team_invites" do
    field :token, :binary

    belongs_to :team, Ferne.Team
    belongs_to :user, Ferne.User

    timestamps(updated_at: false)
  end

  @doc """
  Builds a new invite token and database struct containing the hashed version of the
  token.
  """
  def build_invite(team, user) do
    token = :crypto.strong_rand_bytes(@rand_size)
    hashed_token = :crypto.hash(@hash_algorithm, token)

    {Base.url_encode64(token, padding: false),
     %__MODULE__{
       token: hashed_token,
       team_id: team.id,
       user_id: user.id
     }}
  end

  @doc """
  Checks if the invite token is valid and returns the underlying lookup query.

  The query returns the team ID that the user is to be added to, if any.
  """
  def verify_invite_token_query(token, user) do
    case Base.url_decode64(token, padding: false) do
      {:ok, decoded_token} ->
        hashed_token = :crypto.hash(@hash_algorithm, decoded_token)

        query =
          from(invite in Ferne.TeamInvite,
            where:
              invite.token == ^hashed_token and
                invite.inserted_at > ago(@validity_in_days, "day") and
                invite.user_id == ^user.id,
            select: invite.team_id
          )

        {:ok, query}

      :error ->
        :error
    end
  end

  @doc """
  Gets all invites for a team.
  """
  def get_all_for_team(team) do
    query = from(i in __MODULE__, select: i, where: i.team_id == ^team.id)
    Repo.all(query)
  end

  @doc """
  Creates and sends an invite (email).
  """
  def create_and_send(team, user, url_fun) when is_function(url_fun, 1) do
    {token, invite} = build_invite(team, user)
    Repo.insert(invite)
    Ferne.UserNotifier.deliver_team_invite(user, team, url_fun.(token))
  end

  @doc """
  Confirms an invite. If the given token matches, it deletes the invite and adds the user
  as a team member.
  """
  def confirm(token, user) do
    with {:ok, query} <- verify_invite_token_query(token, user),
         team_id <- Repo.one(query),
         team when not is_nil(team) <- Team.get_by_id(team_id),
         preloaded_team <- Repo.preload(team, [:members]),
         {:ok, %{team: team}} <-
           Repo.transaction(confirm_invite_multi(preloaded_team, user)) do
      {:ok, team}
    else
      _ -> :error
    end
  end

  defp confirm_invite_multi(team, user) do
    delete_query =
      from(invite in __MODULE__,
        select: invite,
        where: invite.team_id == ^team.id and invite.user_id == ^user.id
      )

    added_changeset =
      team |> Ecto.Changeset.change() |> Ecto.Changeset.put_assoc(:members, [user | team.members])

    Ecto.Multi.new()
    |> Ecto.Multi.delete_all(:invite, delete_query)
    |> Ecto.Multi.update(:team, added_changeset)
  end
end
