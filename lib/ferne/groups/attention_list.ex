defmodule Ferne.AttentionList do
  @moduledoc """
  A single entry in a team's "attention list". This can be
  either of these three:

  - Blacklist, which stops the attendee from registering
  - Worrylist, which highlights the attendee in a negative way
  - Favorlist, which highlights the attendee in a positive way
  """
  use Ferne.Schema
  alias Ferne.{Team, User}

  @list_types [:black, :worry, :favor]
  @bulk_re ~r/^[A-Za-z'’-]{2,15}\s[A-Za-z'’‘-]{2,15}\s*(?:,.*)?/m
  @primary_key {:id, :binary_id, autogenerate: true}
  @foreign_key_type :binary_id

  @derive {Jason.Encoder, only: [:ign, :type, :reason]}
  schema "attention_lists" do
    field :ign, :string
    field :type, Ecto.Enum, values: @list_types
    field :reason, :string

    belongs_to :author, User, foreign_key: :inserted_by
    belongs_to :team, Team

    timestamps(type: :utc_datetime, updated_at: false)
  end

  def list_types do
    ["Favor List": "favor", "Worry List": "worry", "Black List": "black"]
  end

  def insert_changeset(list, attrs \\ %{}) do
    list
    |> cast(attrs, ~w(ign type reason inserted_by team_id)a)
    |> validate_required([:ign, :type])
    |> validate_inclusion(:type, Ecto.Enum.values(__MODULE__, :type))
    |> Ferne.Attendee.validate_ign()
  end

  def validate_bulk_structure(bulk) do
    Regex.match?(@bulk_re, bulk)
  end

  def extract_bulk_structure(bulk) do
    Regex.scan(@bulk_re, bulk)
    |> List.flatten()
  end

  def by_team(query \\ __MODULE__, team), do: from(q in query, where: q.team_id == ^team.id)
  def by_type(query \\ __MODULE__, type), do: from(q in query, where: q.type == ^type)
  def by_ign(query \\ __MODULE__, ign), do: from(q in query, where: q.ign == ^ign)

  def get_all_by_team_id(team_id) do
    from(q in __MODULE__, where: [team_id: ^team_id])
    |> Repo.all()
  end
end
