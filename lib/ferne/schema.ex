defmodule Ferne.Schema do
  @moduledoc """
  A meta-module for automatically generating some commonly
  used schema functions.
  """

  defmacro __using__(opts) do
    opts = Keyword.merge([default_sort: :inserted_at], opts)

    quote do
      use Ecto.Schema

      import Ecto
      import Ecto.Changeset
      import Ecto.Query
      alias Ferne.Repo

      def any?(query), do: Repo.count(query) > 0

      def by_position(query \\ __MODULE__), do: from(q in query, order_by: q.position)
      def limit(query \\ __MODULE__, count), do: from(q in query, limit: ^count)

      def newest_first(query \\ __MODULE__, field \\ unquote(opts[:default_sort])),
        do: from(q in query, order_by: [desc: ^field])

      def newest_last(query \\ __MODULE__, field \\ unquote(opts[:default_sort])),
        do: from(q in query, order_by: [asc: ^field])

      def get_by_id(id) do
        Repo.get(__MODULE__, id)
      end

      def delete(item) do
        Repo.delete(item)
      end

      defoverridable(get_by_id: 1, delete: 1)
    end
  end
end
