defmodule Ferne.Attendee do
  @moduledoc """
  Represents someone who signs up for an event. Unique per
  event.
  """
  use Ferne.Schema
  import Ecto.Changeset
  import Ecto.Query
  alias Ferne.Repo
  @primary_key {:id, :binary_id, autogenerate: true}
  @foreign_key_type :binary_id
  @hash_algorithm :blake2b
  @rand_size 32

  @derive {Jason.Encoder, only: [:id, :name, :ign, :email, :selected, :custom_data, :inserted_at]}
  schema "attendees" do
    field :name, :string
    field :ign, :string
    field :email, :string
    field :selected, :boolean, default: false
    field :edit_token, :binary, redact: true
    field :custom_data, :map

    belongs_to :event, Ferne.Event

    timestamps(type: :utc_datetime)
  end

  def creation_changeset(attendee, attrs) do
    attendee
    |> cast(attrs, [:name, :ign, :email])
    |> validate_required([:ign])
    |> validate_ign()
    |> validate_email()
  end

  def change_attendee(attendee, attrs) do
    attendee
    |> cast(attrs, [:custom_data, :selected])
  end

  def generate_hashed_token do
    token = :crypto.strong_rand_bytes(@rand_size)
    hashed_token = :crypto.hash(@hash_algorithm, token)
    {Base.url_encode64(token, padding: false), hashed_token}
  end

  def hash_token(token) do
    :crypto.hash(@hash_algorithm, token)
  end

  def validate_ign(attendee) do
    attendee
    |> validate_format(:ign, ~r/^[A-Za-z'’‘-]{2,15}\s[A-Za-z'’‘-]{2,15}$/,
      message:
        "please use a valid FFXIV character name. check if you have an extra space somewhere?"
    )
  end

  def validate_email(attendee) do
    attendee
    |> validate_format(:email, ~r/^[^\s]+@[^\s]+$/, message: "must have the @ sign and no spaces")
    |> validate_length(:email, max: 160)
  end

  @doc """
  Fetches an attendee by edit token.
  """
  def get_by_edit_token(token) do
    from(a in __MODULE__, select: a, where: a.edit_token == ^token)
    |> Repo.one()
  end

  @doc """
  Selects an attendee.
  """
  def select(attendee) do
    change_attendee(attendee, %{selected: true})
    |> Repo.update()
  end

  @doc """
  Unselects an attendee.
  """
  def unselect(attendee) do
    change_attendee(attendee, %{selected: false})
    |> Repo.update()
  end

  @doc """
  Checks if a signup for this event already occurred with the given email or IGN.
  """
  def is_signup_unique?(%{changes: %{event_id: event_id, ign: ign}} = attendee) do
    query =
      if Map.has_key?(attendee.changes, :email) do
        from(a in __MODULE__,
          where: a.email == ^attendee.changes.email and a.event_id == ^event_id
        )
      else
        from(a in __MODULE__, where: a.ign == ^ign and a.event_id == ^event_id)
      end

    case Repo.one(query) do
      nil -> true
      _ -> false
    end
  end
end
