defmodule Ferne.EventTemplate do
  @moduledoc """
  Represents a template out of which new events can be created.
  """
  use Ferne.Schema
  import Ecto.Changeset
  import Ecto.Query
  alias Ferne.{Event, Block, Repo}

  @primary_key {:id, :binary_id, autogenerate: true}
  @foreign_key_type :binary_id

  schema "event_templates" do
    field :name, :string
    field :event_data, :map
    field :block_data, :map

    belongs_to :team, Ferne.Team

    timestamps(type: :utc_datetime, updated_at: false)
  end

  def creation_changeset(template, attrs \\ %{}) do
    template
    |> cast(attrs, [:name, :team_id])
    |> validate_required([:name, :team_id])
  end

  def by_team(query \\ __MODULE__, team), do: from(query, where: [team_id: ^team.id])

  @doc """
  Creates a new template. Takes an event, attributes (name and team ID), and from that,
  extrapolates all of the data that it can get from the event into an event template.
  """
  def create(attrs, event) do
    event_with_blocks = Repo.preload(event, [:blocks])

    event_data = %{
      name: event.name,
      description: event.description,
      location: event.location,
      has_attendee_limit: event.has_attendee_limit,
      attendee_limit: event.attendee_limit,
      store_email: event.store_email,
      store_name: event.store_name
    }

    block_data =
      Map.new(event_with_blocks.blocks, fn block ->
        {block.identifier, block.data}
      end)

    changeset =
      creation_changeset(%__MODULE__{}, attrs)
      |> Ecto.Changeset.change(%{event_data: event_data, block_data: block_data})

    Repo.insert(changeset)
  end

  @doc """
  Creates a new event based off a template.
  """
  def create_event_from_template(template, user, attrs) do
    event_changeset =
      Event.creation_changeset(
        %Event{},
        Map.merge(
          %{
            "name" => template.event_data["name"],
            "description" => template.event_data["description"],
            "location" => template.event_data["location"],
            "attendee_limit" => template.event_data["attendee_limit"],
            "store_email" => template.event_data["store_email"],
            "store_name" => template.event_data["store_name"],
            "team_id" => template.team_id,
            "public" => false
          },
          attrs
        )
      )
      |> Ecto.Changeset.change(%{user_id: user.id})

    with {:ok, event} <- Repo.insert(event_changeset),
         multi <- event_template_block_multi(template, event),
         {:ok, _} <- Repo.transaction(multi) do
      {:ok, event}
    end
  end

  defp event_template_block_multi(template, event) do
    Enum.reduce(template.block_data, Ecto.Multi.new(), fn {id, data}, acc ->
      changeset =
        Block.change_block(%Block{}, %{
          identifier: id,
          data: data
        })
        |> Ecto.Changeset.change(%{event_id: event.id})

      Ecto.Multi.insert(acc, id, changeset)
    end)
  end
end
