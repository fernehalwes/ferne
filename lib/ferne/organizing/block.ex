defmodule Ferne.Block do
  @moduledoc """
  A block is a bit of extra functionality that can be added to events.
  There's a list of "types" of blocks, such as one that adds support for
  custom fields to an event, but they're saved separately as database entries
  (e.g. one block for every "custom fields" block per event), because they all
  hold different data.
  """
  use Ferne.Schema
  import Ecto.Changeset
  @primary_key {:id, :binary_id, autogenerate: true}
  @foreign_key_type :binary_id
  @valid_ids ["custom_fields", "discord_login", "ffxiv_roles", "parties", "titles"]

  @derive {Jason.Encoder, only: [:identifier, :data]}
  schema "blocks" do
    field :identifier, :string
    field :data, :map
    belongs_to :event, Ferne.Event

    timestamps()
  end

  def change_block(block, attrs \\ %{}) do
    block
    |> cast(attrs, [:identifier, :data])
    |> validate_inclusion(:identifier, @valid_ids)
  end

  def change_block_data(block, data) do
    wrapped_data = %{data: %{data: data}}

    block
    |> cast(wrapped_data, [:data])
  end

  def valid_ids do
    @valid_ids
  end

  @doc "Returns the block's identifier"
  @callback id() :: String.t()

  @doc "Returns the block's name"
  @callback name() :: String.t()

  @doc "Returns a description of what the block does"
  @callback description() :: String.t()

  @doc "Returns whether a block has a settings page or not"
  @callback has_settings() :: boolean()

  @doc "Returns the metadata for a block"
  @callback get_metadata!(Map.t()) :: List.t()

  @doc "Returns whether a block adds something to the signup page."
  @callback extends_signup_page() :: boolean()

  @doc "Returns whether a block interacts with attendee data."
  @callback interacts_with_attendee() :: boolean()

  @doc "Returns whether a block has a special section on the manage page, and an associated sub-page."
  @callback has_special_section() :: boolean()

  # Optional callbacks
  @doc "Returns the special section's name"
  @callback special_section_name() :: String.t()

  @doc "Returns the special section's icon"
  @callback special_section_icon() :: String.t()

  @doc "Returns the optional block struct's types"
  @callback types() :: Map.t()

  @doc "Converts a map into a changeset for that block and validates it"
  @callback change(Ecto.Changeset.t(), any()) :: Ecto.Changeset.t()

  @doc "Controller GET handler for the block settings page"
  @callback controller_get(Connection.t(), Ferne.Event.t()) :: Connection.t()

  @doc "Controller POST handler for the block settings page"
  @callback controller_post(Connection.t(), Ferne.Event.t(), Map.t()) :: Connection.t()

  @doc "Controller GET handler for the block special page"
  @callback controller_special_get(Connection.t(), Ferne.Event.t()) :: Connection.t()

  @doc "Controller POST handler for the block special page"
  @callback controller_special_post(Connection.t(), Ferne.Event.t(), Map.t()) ::
              Connection.t()

  @optional_callbacks special_section_name: 0,
                      special_section_icon: 0,
                      types: 0,
                      controller_get: 2,
                      controller_post: 3,
                      controller_special_get: 2,
                      controller_special_post: 3,
                      change: 2
end
