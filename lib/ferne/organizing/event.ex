defmodule Ferne.Event do
  @moduledoc """
  Represents a single event.
  """
  use Ferne.Schema
  import Ecto.Query
  import Ecto.Changeset
  alias Ferne.Repo
  @primary_key {:id, :binary_id, autogenerate: true}
  @foreign_key_type :binary_id
  @behaviour Bodyguard.Policy

  @derive {Jason.Encoder,
           only: [
             :name,
             :description,
             :time,
             :location,
             :store_email,
             :store_name,
             :attendee_limit,
             :state,
             :attendees,
             :blocks,
             :team_members
           ]}
  schema "events" do
    field :name, :string
    field :description, :string
    field :time, :utc_datetime
    field :location, :string
    field :public, :boolean, default: false
    field :store_email, :boolean, default: true
    field :store_name, :boolean, default: true
    field :has_attendee_limit, :boolean, virtual: true
    field :attendee_limit, :integer
    field :state, :string, default: "planned"

    belongs_to :user, Ferne.User
    belongs_to :team, Ferne.Team
    has_many :event_organizers, Ferne.EventOrganizer
    has_many :organizers, through: [:event_organizers, :user]
    has_many :team_organizers, through: [:team, :team_members]
    has_many :team_members, through: [:team, :members]
    has_many :attendees, Ferne.Attendee, on_delete: :delete_all
    has_many :blocks, Ferne.Block, on_delete: :delete_all

    timestamps(type: :utc_datetime)
  end

  ## Authorization rules
  # Primary organizers, team organizers/admins and co-organizers can manage their events
  # Primary organizer
  def authorize(:manage_event, %{id: user_id} = _user, %{user_id: user_id} = _event), do: :ok

  # Team organizer/admin
  def authorize(:manage_event, %{id: user_id} = _user, %{
        team_organizers: tos,
        organizers: []
      }) do
    entry = Enum.find(tos, &(&1.user_id == user_id))
    if entry && (entry.admin || entry.superadmin), do: :ok, else: :error
  end

  # Co-organizer OR team organizer/admin
  def authorize(:manage_event, %{id: user_id} = _user, %{
        team: t,
        team_organizers: tos,
        organizers: organizers
      })
      when not is_nil(t) do
    team_member = !is_nil(t) and Enum.find(tos, &(&1.user_id == user_id))

    is_organizer = organizers |> Enum.map(& &1.id) |> Enum.member?(user_id)
    team_member.admin or team_member.superadmin or is_organizer
  end

  def authorize(:manage_event, %{id: _user_id} = _user, %{organizers: []}), do: :error

  # Co-organizer
  def authorize(:manage_event, %{id: user_id} = _user, %{organizers: organizers}) do
    if(
      organizers
      |> Enum.map(& &1.id)
      |> Enum.member?(user_id),
      do: :ok
    )
  end

  def authorize(:manage_event, _, _), do: :error

  # Only the event creator can do destructive actions on an event (add organizers or cancel)
  def authorize(:mutate_event, %{id: user_id} = _user, %{user_id: user_id} = _event), do: :ok
  def authorize(:mutate_event, _, _), do: :error

  # All team members can view the event management page
  def authorize(:view_manage_event, %{id: user_id} = _user, %{user_id: user_id} = _event), do: :ok

  def authorize(:view_manage_event, %{id: user_id} = _user, %{
        team: t,
        team_organizers: tos,
        organizers: []
      })
      when not is_nil(t) do
    !!Enum.find(tos, &(&1.user_id == user_id))
  end

  def authorize(:view_manage_event, %{id: user_id} = _user, %{
        team: t,
        team_organizers: tos,
        organizers: organizers
      }) do
    is_team_member = !is_nil(t) and !!Enum.find(tos, &(&1.user_id == user_id))
    is_organizer = organizers |> Enum.map(& &1.id) |> Enum.member?(user_id)
    is_team_member or is_organizer
  end

  def authorize(:view_manage_event, _, _), do: :error

  def creation_changeset(event, attrs) do
    event
    |> cast(attrs, [
      :name,
      :description,
      :time,
      :location,
      :attendee_limit,
      :store_email,
      :store_name,
      :team_id,
      :public
    ])
    |> validate_required([:name, :time])
  end

  def state_changeset(event, state) do
    event
    |> cast(%{state: state}, [:state])
    |> validate_inclusion(:state, ["planned", "closed", "selected", "cancelled"])
  end

  def by_team(query \\ __MODULE__, team), do: from(query, where: [team_id: ^team.id])
  def by_public(query \\ __MODULE__), do: from(query, where: [public: true])

  @doc """
  Gets a single event by ID.
  """
  def get_by_id(id) do
    super(id)
    |> Repo.preload(:attendees)
  end

  @doc """
  Get all events where a specific user is involved.
  """
  def get_involved_for_user(user) do
    query =
      from(e in __MODULE__,
        select: e,
        left_join: tm in assoc(e, :team_organizers),
        on: tm.user_id == ^user.id and tm.team_id == e.team_id,
        left_join: eo in assoc(e, :event_organizers),
        on: eo.event_id == e.id,
        where: e.user_id == ^user.id or eo.user_id == ^user.id or tm.user_id == ^user.id
      )

    Repo.all(query)
  end

  @doc """
  Gets all attendees for the given event.
  """
  def get_attendees(event) do
    Ecto.assoc(event, :attendees)
    |> Repo.all()
  end

  @doc """
  Creates an event.
  """
  def create(attrs, user) do
    %__MODULE__{}
    |> creation_changeset(attrs)
    |> Ecto.Changeset.change(%{user_id: user.id})
    |> Repo.insert()
  end

  @doc """
  Updates an event.
  """
  def update(event, attrs) do
    creation_changeset(event, attrs)
    |> Repo.update()
  end
end
