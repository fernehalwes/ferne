defmodule Ferne.EventOrganizer do
  @moduledoc """
  Join schema between events and organizers.
  """
  use Ferne.Schema
  import Ecto.Query
  import Ecto.Changeset
  alias Ferne.Repo
  @primary_key {:id, :binary_id, autogenerate: true}
  @foreign_key_type :binary_id

  schema "event_organizers" do
    belongs_to :event, Ferne.Event
    belongs_to :user, Ferne.User
    field :role, :string

    timestamps(type: :utc_datetime)
  end

  def creation_changeset(eo, attrs) do
    eo
    |> cast(attrs, [:event_id, :user_id, :role])
    |> validate_required([:event_id, :user_id])
  end

  def create(attrs, user, event) do
    %__MODULE__{}
    |> Ecto.Changeset.change(%{user_id: user.id, event_id: event.id})
    |> creation_changeset(attrs)
    |> Repo.insert()
  end

  def delete(user, event) do
    query =
      from(ev in __MODULE__,
        where: ev.user_id == ^user.id and ev.event_id == ^event.id
      )

    evorg = Repo.one(query)
    Repo.delete(evorg)
  end
end
