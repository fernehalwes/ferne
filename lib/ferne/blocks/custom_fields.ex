defmodule Ferne.BlockDb.CustomFields do
  @moduledoc """
  A block that allows organizers to set custom "fields"
  for an event, either ones that they can edit via the admin
  interface or ones that can be filled in by the attendees when
  they're signing up.
  """
  @behaviour Ferne.Block

  def id, do: "custom_fields"
  def name, do: "Custom Fields"

  def has_settings, do: true
  def has_special_section, do: false
  def extends_signup_page, do: true
  def interacts_with_attendee, do: false

  def description do
    "Add custom fields to classify attendees with."
  end

  def controller_get(conn, event) do
    Phoenix.LiveView.Controller.live_render(conn, FerneWeb.CustomFieldsLive,
      session: %{
        "event" => event
      }
    )
  end

  def get_metadata!(data) do
    if data do
      Enum.reduce(data, [], fn el, acc ->
        [
          %{
            name: el["name"],
            short_name: if(Map.has_key?(el, "short_name"), do: el["short_name"], else: nil),
            custom: true,
            id: el["id"],
            editable: !el["has_form"],
            required: el["required"],
            type: el["type"],
            description: el["description"],
            choices: el["choices"]
          }
          | acc
        ]
      end)
      |> Enum.reverse()
    else
      []
    end
  end

  @doc """
  Given a field and a custom field body, returns the human-readable value of the field.
  """
  def get_field_value(%{"type" => "boolean"} = _field, body) do
    if(body, do: "Yes", else: "No")
  end

  def get_field_value(%{"type" => "single_select", "choices" => choices} = _field, body) do
    Enum.find(choices, fn %{"id" => id} ->
      id == body
    end)["value"]
  end

  def get_field_value(%{"type" => "multi_select", "choices" => choices} = _field, body) do
    if is_list(body),
      do:
        Enum.map_join(
          body,
          ", ",
          &Enum.find(choices, fn %{"id" => id} -> id == &1 end)["value"]
        ),
      else: nil
  end

  def get_field_value(_field, body), do: body
end
