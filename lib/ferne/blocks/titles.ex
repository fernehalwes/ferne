defmodule Ferne.BlockDb.Titles do
  @moduledoc """
  A block that allows organizers to assign "titles" to people,
  meaning that they can type in a string of text behind
  their name that appears anywhere their name
  would normally appear.
  """
  @behaviour Ferne.Block

  def id, do: "titles"
  def name, do: "Titles"

  def has_settings, do: false
  def has_special_section, do: false
  def extends_signup_page, do: false
  def interacts_with_attendee, do: false

  def description do
    "Assign attendees a title that displays next to their name."
  end

  def get_metadata!(_) do
    [
      %{
        name: "Title",
        id: "title",
        editable: true,
        custom: false
      }
    ]
  end
end
