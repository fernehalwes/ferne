defmodule Ferne.BlockDb.FFXIVRoles do
  @moduledoc """
  Lets attendees assign FFXIV classes to themselves.
  """
  alias Ferne.BlockDb
  import Phoenix.Controller
  import Ecto.Changeset
  @behaviour Ferne.Block

  @derive {Jason.Encoder, only: [:name, :description, :roles_only, :restrict_roles]}
  defstruct [:name, :description, :roles_only, :restrict_roles]
  def id, do: "ffxiv_roles"
  def name, do: "FFXIV Roles"

  def has_settings, do: true
  def has_special_section, do: false
  def extends_signup_page, do: true
  def interacts_with_attendee, do: false

  def description do
    "Allows attendees to assign themselves FFXIV classes/jobs."
  end

  def types do
    %{name: :string, description: :string, roles_only: :boolean, restrict_roles: :string}
  end

  def controller_get(conn, event) do
    block = BlockDb.get_block_for_event("ffxiv_roles", event)

    changeset =
      {%Ferne.BlockDb.FFXIVRoles{}, types()}
      |> cast(block.data["data"] || %{}, Map.keys(types()))

    put_view(conn, FerneWeb.BlockView)
    |> render("ffxiv_roles.html",
      event: event,
      block: block,
      changeset: changeset,
      page_title: "Edit FFXIV Roles"
    )
  end

  def controller_post(conn, event, params) do
    block = BlockDb.get_block_for_event("ffxiv_roles", event)
    %{"ffxiv_roles" => data_params} = params

    changeset =
      {%Ferne.BlockDb.FFXIVRoles{}, types()}
      |> cast(data_params, Map.keys(types()))

    if changeset.valid? do
      Ferne.Block.change_block_data(block, apply_changes(changeset))
      |> Ferne.Repo.update()

      put_flash(conn, :info, "Settings successfully updated!")
    end

    if(changeset.valid?,
      do: put_flash(conn, :info, "Settings successfully updated!"),
      else: put_flash(conn, :error, "Error while updating settings!")
    )
    |> put_view(FerneWeb.BlockView)
    |> render("ffxiv_roles.html",
      event: event,
      block: block,
      changeset: changeset,
      page_title: "Edit FFXIV Roles"
    )
  end

  def get_metadata!(_data) do
    [
      %{
        name: "Roles",
        id: "ffxiv_roles",
        editable: false,
        custom: false
      }
    ]
  end
end
