defmodule Ferne.BlockDb.DiscordLogin do
  @moduledoc """
  A block that allows events to gate signups with logging
  in via Discord, auto-filling the attendees username in the
  process.
  """
  @behaviour Ferne.Block

  def id, do: "discord_login"
  def name, do: "Discord Login"

  def has_settings, do: false
  def has_special_section, do: false
  def extends_signup_page, do: true
  def interacts_with_attendee, do: false

  def description do
    "Forces attendees to provide their Discord usernames via logging in with Discord."
  end

  def get_metadata!(_) do
    [
      %{
        name: "Discord",
        id: "discord_login",
        editable: false,
        custom: false
      },
      %{
        name: "Discord (legacy)",
        id: "discord_username",
        editable: false,
        custom: false
      }
    ]
  end
end
