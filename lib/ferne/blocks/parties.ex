defmodule Ferne.BlockDb.Parties do
  @moduledoc """
  Lets you split attendees into parties.
  """
  @behaviour Ferne.Block
  import Ecto.Changeset
  alias FerneWeb.Roles

  @derive {Jason.Encoder, only: [:id, :name, :size, :tanks, :dps, :healers, :excluded]}
  defstruct [:id, :name, :size, :tanks, :dps, :healers, :excluded]
  def id, do: "parties"
  def name, do: "Parties"

  def has_settings, do: false
  def has_special_section, do: false
  def extends_signup_page, do: false
  def interacts_with_attendee, do: true

  def description do
    # TODO: Merge these strings when auto-select is implemented
    "Organize your attendees into parties." <>
      if(false, do: " Supports auto-selecting attendees based on their preferred roles.", else: "")
  end

  def types do
    %{
      id: :string,
      name: :string,
      size: :integer,
      tanks: :integer,
      dps: :integer,
      healers: :integer,
      excluded: :boolean
    }
  end

  def change(changeset \\ %Ferne.BlockDb.Parties{}, attrs) do
    {changeset, types()}
    |> cast(attrs, Map.keys(types()))
    |> validate_required([:name])
  end

  def get_metadata!(data) do
    [
      %{name: "Party", id: "party", editable: false, custom: false, parties: data}
    ]
  end

  def group_attendees_by_party(attendees, parties) do
    party_ids = Enum.map(parties, & &1.id)

    groups =
      Enum.group_by(attendees, fn att ->
        if att.custom_data["party"] && Enum.member?(party_ids, att.custom_data["party"]["id"]) do
          att.custom_data["party"]["id"]
        else
          "no_party"
        end
      end)

    sorted_rest =
      groups
      |> Map.delete("no_party")
      |> Enum.map(&sort_party_by_role/1)
      |> Enum.into(%{})

    sorted_rest
    |> Map.put("no_party", groups["no_party"])
  end

  def sort_parties_by_size(parties, groups) do
    Enum.sort(parties, fn p1, p2 ->
      cond do
        !groups[p1.id] -> true
        !groups[p2.id] -> false
        length(groups[p1.id]) > length(groups[p2.id]) -> false
        true -> true
      end
    end)
  end

  def sort_party_by_role({id, attendees}) do
    {id,
     Enum.sort(attendees, fn att1, att2 ->
       if att1.custom_data["party"]["role"] && att2.custom_data["party"]["role"] do
         role1 = att1.custom_data["party"]["role"]
         role2 = att2.custom_data["party"]["role"]

         party_sort_cond(role1, role2)
       else
         att1.ign >= att2.ign
       end
     end)}
  end

  defp party_sort_cond(role1, role2) do
    cond do
      Roles.is_healer(role1) && Roles.is_tank(role2) -> false
      Roles.is_shhealer(role1) && Roles.is_rghealer(role2) -> false
      Roles.is_dps(role1) && Roles.is_healer(role2) -> false
      Roles.is_dps(role1) && Roles.is_tank(role2) -> false
      true -> true
    end
  end
end
