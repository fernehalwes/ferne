defmodule FerneWeb.EventView do
  use FerneWeb, :view

  def get_event_time(event) do
    Timex.format!(event.time, "{D} {Mshort} {YYYY} {h24}:{m} ST")
  end

  @doc """
  Returns a tuple list with the organizer in the first spot, and their
  event_organizer entity in the second spot. Includes yourself, too.
  """
  def get_all_organizers(event) do
    [
      {event.user, %{role: "Creator"}}
      | Enum.map(event.organizers, fn org ->
          evorg = Enum.find(event.event_organizers, &(&1.user_id == org.id))
          {org, evorg}
        end)
    ]
  end

  @doc """
  Build a one-stage breadcrumb element.
  """
  def build_event_breadcrumbs_one(conn, event) do
    """
      <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
          <li class="breadcrumb-item"><a href="#{Routes.page_path(conn, :index)}">Events</a></li>
          <li class="breadcrumb-item active">#{event.name}</li>
        </ol>
      </nav>
    """
  end

  @doc """
  Build two-stage event breadcrumb HTML.
  """
  def build_event_breadcrumbs_two(conn, event, name) do
    """
      <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
          <li class="breadcrumb-item"><a href="#{Routes.page_path(conn, :index)}">Events</a></li>
          <li class="breadcrumb-item"><a href="#{Routes.event_path(conn, :show_manage, event.id)}">#{event.name}</a></li>
          <li class="breadcrumb-item active" aria-current="page">#{name}</li>
        </ol>
      </nav>
    """
  end
end
