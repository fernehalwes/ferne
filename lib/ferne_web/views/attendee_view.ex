defmodule FerneWeb.AttendeeView do
  use FerneWeb, :view
  alias FerneWeb.Roles

  @doc """
  Renders either a change or a data field from a changeset.
  """
  def get_data_or_change(changeset, id) do
    cond do
      Map.has_key?(changeset.changes, :custom_data) -> changeset.changes.custom_data[id]
      Map.has_key?(changeset.data, :custom_data) -> changeset.data.custom_data[id]
      true -> nil
    end
  end

  def get_bool_data_or_change(changeset, id) do
    cond do
      Map.has_key?(changeset.changes, :custom_data) -> changeset.changes.custom_data[id] == "Yes"
      Map.has_key?(changeset.data, :custom_data) -> changeset.data.custom_data[id] == "Yes"
      true -> nil
    end
  end

  def get_sselect_data_or_change(changeset, id, fid) do
    cond do
      Map.has_key?(changeset.changes, :custom_data) -> changeset.changes.custom_data[id] == fid
      Map.has_key?(changeset.data, :custom_data) -> changeset.data.custom_data[id] == fid
      true -> nil
    end
  end

  def get_mselect_data_or_change(changeset, id, fid) do
    cond do
      Map.has_key?(changeset.changes, :custom_data) ->
        changeset.changes.custom_data[id] && Enum.member?(changeset.changes.custom_data[id], fid)

      Map.has_key?(changeset.data, :custom_data) ->
        changeset.data.custom_data[id] && Enum.member?(changeset.data.custom_data[id], fid)

      true ->
        nil
    end
  end

  @doc """
  Returns the correctly formatted header value for a table field.
  """
  def display_field_value(header, attendee, body) do
    cond do
      is_nil(body[header.id][attendee.id]) ->
        ""

      header.custom ->
        display_field_value_sub(header.type, header, attendee, body)

      header.id == "ffxiv_roles" ->
        render_roles_str(body[header.id][attendee.id])

      header.id == "party" ->
        render_party_str(body[header.id][attendee.id], header)

      header.id == "discord_login" ||
          header.id == "discord_username" ->
        render_discord(body[header.id][attendee.id])

      true ->
        body[header.id][attendee.id]
    end
  end

  @doc """
  Returns the correct string for a string of FFXIV roles.
  """
  def render_roles_str(roles) do
    list = roles |> String.trim() |> String.split(",")

    Enum.map_join(list, ", ", &Roles.get_role_name/1)
  end

  @doc """
  Returns HTML for the assigned party of an attendee.
  """
  def render_party_str(party, header) do
    if party && party["id"] do
      matching_party = Enum.find(header.parties, &(&1["id"] == party["id"]))

      "#{Roles.get_role_name(party["role"])} in #{matching_party["name"]}"
    else
      ""
    end
  end

  def render_discord(meta) do
    if is_map(meta) do
      meta["username"]
    else
      meta
    end
  end

  defp display_field_value_sub("boolean", header, attendee, body) do
    if(body[header.id][attendee.id], do: "Yes", else: "No")
  end

  defp display_field_value_sub("single_select", header, attendee, body) do
    Enum.find(header.choices, fn %{"id" => id} ->
      id == body[header.id][attendee.id]
    end)["value"]
  end

  defp display_field_value_sub("multi_select", header, attendee, body) do
    if is_list(body[header.id][attendee.id]),
      do:
        Enum.map_join(
          body[header.id][attendee.id],
          ", ",
          &Enum.find(header.choices, fn %{"id" => id} -> id == &1 end)["value"]
        ),
      else: nil
  end

  defp display_field_value_sub(_, header, attendee, body), do: body[header.id][attendee.id]
end
