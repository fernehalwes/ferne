defmodule FerneWeb.PageView do
  use FerneWeb, :view

  def sort_events_by_date(events) do
    Enum.sort(events, &(DateTime.compare(&1.time, &2.time) == :lt))
  end
end
