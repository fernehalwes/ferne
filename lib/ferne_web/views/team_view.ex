defmodule FerneWeb.TeamView do
  @moduledoc """
  View module for teams.
  """
  use FerneWeb, :view

  def pair_team_members(team) do
    Enum.map(team.members, fn member ->
      join_struct = Enum.find(team.team_members, &(&1.user_id == member.id))
      {member, join_struct}
    end)
  end
end
