defmodule FerneWeb.LayoutView do
  use FerneWeb, :view

  # Phoenix LiveDashboard is available only in development by default,
  # so we instruct Elixir to not warn if the dashboard route is missing.
  @compile {:no_warn_undefined, {Routes, :live_dashboard_path, 2}}
  @dialyzer {:nowarn_function, prod?: 0}

  def get_environment_name do
    System.get_env("MIX_ENV", "development")
  end

  def prod? do
    get_environment_name() == "prod"
  end
end
