defmodule FerneWeb.AttentionListView do
  @moduledoc """
  View module for attention lists.
  """
  use FerneWeb, :view
end
