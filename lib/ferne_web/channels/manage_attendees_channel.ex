defmodule FerneWeb.ManageAttendeesChannel do
  @moduledoc false
  use FerneWeb, :channel
  alias Ferne.{Event, Repo, AttentionList, BlockDb, Attendee, Block}
  alias Ferne.BlockDb.Parties
  alias Ecto.Multi

  def join("manage:" <> event_id, _params, socket) do
    {event, lists} = fetch_event(event_id)

    edit_permission =
      Bodyguard.permit?(Event, :manage_event, %{id: socket.assigns.user_id}, event)

    {:ok, %{event: event, lists: lists, edit_permission: edit_permission},
     assign(socket, event: event)}
  end

  def handle_in(
        "set_custom_data_field",
        %{"uid" => uid, "field" => field, "value" => value},
        socket
      ) do
    if Bodyguard.permit?(
         Event,
         :manage_event,
         %{id: socket.assigns.user_id},
         socket.assigns.event
       ) do
      attendee = Attendee.get_by_id(uid)

      custom_data =
        attendee.custom_data
        |> Map.put(field, value)

      changeset = Attendee.change_attendee(attendee, %{custom_data: custom_data})
      Repo.update(changeset)
      {new_event, _} = fetch_event(socket.assigns.event.id)
      {:reply, {:ok, new_event}, assign(socket, event: new_event)}
    else
      {:noreply, socket}
    end
  end

  def handle_in("new_party", %{"name" => name}, socket) do
    if Bodyguard.permit?(
         Event,
         :manage_event,
         %{id: socket.assigns.user_id},
         socket.assigns.event
       ) do
      block = BlockDb.find_block(socket.assigns.event.blocks, "parties")
      party = %{name: name, id: Ecto.UUID.generate(), size: 8, dps: 4, healers: 2, tanks: 2}
      changeset = Parties.change(party)

      if changeset.valid? do
        new_parties = (block.data["data"] || []) ++ [Ecto.Changeset.apply_changes(changeset)]

        Block.change_block_data(block, new_parties)
        |> Repo.update()

        {new_event, _} = fetch_event(socket.assigns.event.id)
        {:reply, {:ok, new_event}, assign(socket, event: new_event)}
      end
    else
      {:noreply, socket}
    end
  end

  def handle_in("delete_party", %{"id" => id}, socket) do
    if Bodyguard.permit?(
         Event,
         :manage_event,
         %{id: socket.assigns.user_id},
         socket.assigns.event
       ) do
      new_attendees = Event.get_attendees(socket.assigns.event)
      block = BlockDb.find_block(socket.assigns.event.blocks, "parties")
      parties = block.data["data"] |> Enum.map(&Ecto.Changeset.apply_changes(Parties.change(&1)))
      groups = Parties.group_attendees_by_party(new_attendees, parties)

      members = groups[id] || []

      multi = Multi.new()
      idx = Enum.find_index(parties, &(&1.id == id))
      new_parties = List.delete_at(parties, idx)

      Enum.reduce(members, multi, fn att, acc ->
        Multi.update(
          acc,
          String.to_atom("update#{String.split(att.id, "-") |> Enum.at(0)}"),
          Attendee.change_attendee(att, %{
            custom_data: Map.delete(att.custom_data, "party")
          })
        )
      end)
      |> Multi.update(
        :parties,
        Block.change_block_data(
          block,
          new_parties
        )
      )
      |> Ferne.Repo.transaction()

      {new_event, _} = fetch_event(socket.assigns.event.id)
      {:reply, {:ok, new_event}, assign(socket, event: new_event)}
    else
      {:noreply, socket}
    end
  end

  def handle_in("confirm", %{"attIds" => ids}, socket) do
    if Bodyguard.permit?(
         Event,
         :manage_event,
         %{id: socket.assigns.user_id},
         socket.assigns.event
       ) do
      Enum.each(ids, fn id ->
        attendee = Enum.find(socket.assigns.event.attendees, &(&1.id == id))
        Attendee.select(attendee)
      end)

      {new_event, _} = fetch_event(socket.assigns.event.id)
      {:reply, {:ok, new_event}, assign(socket, event: new_event)}
    else
      {:noreply, socket}
    end
  end

  def handle_in("unconfirm", %{"attIds" => ids}, socket) do
    if Bodyguard.permit?(
         Event,
         :manage_event,
         %{id: socket.assigns.user_id},
         socket.assigns.event
       ) do
      Enum.each(ids, fn id ->
        attendee = Enum.find(socket.assigns.event.attendees, &(&1.id == id))
        Attendee.unselect(attendee)
      end)

      {new_event, _} = fetch_event(socket.assigns.event.id)
      {:reply, {:ok, new_event}, assign(socket, event: new_event)}
    else
      {:noreply, socket}
    end
  end

  def handle_in("confirm_all_in_party", _params, socket) do
    if Bodyguard.permit?(
         Event,
         :manage_event,
         %{id: socket.assigns.user_id},
         socket.assigns.event
       ) do
      new_attendees = Event.get_attendees(socket.assigns.event)
      block = BlockDb.find_block(socket.assigns.event.blocks, "parties")
      parties = block.data["data"] |> Enum.map(&Ecto.Changeset.apply_changes(Parties.change(&1)))
      groups = Parties.group_attendees_by_party(new_attendees, parties)
      to_confirm = Map.delete(groups, "no_party") |> Map.values() |> List.flatten()

      multi = Multi.new()

      Enum.reduce(to_confirm, multi, fn att, acc ->
        Multi.update(
          acc,
          String.to_atom("update#{String.split(att.id, "-") |> Enum.at(0)}"),
          Attendee.change_attendee(att, %{selected: true})
        )
      end)
      |> Repo.transaction()

      {new_event, _} = fetch_event(socket.assigns.event.id)
      {:reply, {:ok, new_event}, assign(socket, event: new_event)}
    else
      {:noreply, socket}
    end
  end

  def handle_in("edit", %{"aid" => id}, socket) do
    {:reply, {:ok, Routes.attendee_url(socket, :edit, "faketoken", override_id: id)}, socket}
  end

  def handle_in("delete", %{"aid" => id}, socket) do
    if Bodyguard.permit?(
         Event,
         :manage_event,
         %{id: socket.assigns.user_id},
         socket.assigns.event
       ) do
      attendee = Enum.find(socket.assigns.event.attendees, &(&1.id == id))
      Repo.delete(attendee)
      {new_event, _} = fetch_event(socket.assigns.event.id)
      {:reply, {:ok, new_event}, assign(socket, event: new_event)}
    else
      {:noreply, socket}
    end
  end

  defp fetch_event(id) do
    evt =
      Event.get_by_id(id)
      |> Repo.preload([:attendees, :blocks, :organizers, :team_organizers, :team_members])

    lists =
      if evt.team_id do
        AttentionList.by_team(%{id: evt.team_id})
        |> Repo.all()
      else
        nil
      end

    {evt, lists}
  end
end
