defmodule FerneWeb.EventSocket do
  @moduledoc false
  use Phoenix.Socket

  channel "manage:*", FerneWeb.ManageAttendeesChannel

  def connect(params, socket, _connect_info) do
    {:ok, assign(socket, user_id: params["userId"])}
  end

  def id(socket), do: "event:#{socket.assigns.user_id}"
end
