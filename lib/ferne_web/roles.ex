defmodule FerneWeb.Roles do
  @moduledoc """
  Misc. role-related functions that may need to be shared between various components.
  """

  @tanks ["paladin", "warrior", "darkknight", "gunbreaker"]
  @rghealers ["whitemage", "astrologian"]
  @shhealers ["scholar", "sage"]
  @healers ["whitemage", "scholar", "astrologian", "sage"]
  @melee ["monk", "samurai", "ninja", "dragoon", "reaper"]
  @physranged ["bard", "machinist", "dancer"]
  @magicranged ["blackmage", "summoner", "redmage"]
  @dps Enum.concat([@melee, @physranged, @magicranged])

  def expand_roles(roles) do
    Enum.map(roles, fn role ->
      case role do
        "tank" -> @tanks
        "healer" -> @healers
        "rghealer" -> @rghealers
        "shhealer" -> @shhealers
        "melee" -> @melee
        "physranged" -> @physranged
        "magicranged" -> @magicranged
        _ -> role
      end
    end)
    |> List.flatten()
    |> Enum.uniq()
  end

  def get_role_type(role) do
    case role do
      r when r in @tanks -> "tank"
      r when r in @healers -> "healer"
      r when r in @dps -> "dps"
      r when r in ["shhealer", "rghealer"] -> "healer"
      r when r in ["melee", "physranged", "magicranged"] -> "dps"
      _ -> role
    end
  end

  def all_jobs do
    Enum.concat([@tanks, @healers, @dps])
  end

  def all_roles do
    ["tank", "rghealer", "shhealer", "melee", "physranged", "magicranged"]
  end

  def is_tank(role) do
    Enum.member?(@tanks, role) || role == "tank"
  end

  def is_shhealer(role) do
    Enum.member?(@shhealers, role) || role == "shhealer"
  end

  def is_rghealer(role) do
    Enum.member?(@rghealers, role) || role == "rghealer"
  end

  def is_healer(role) do
    Enum.member?(@healers, role) || Enum.member?(["rghealer", "shhealer", "healer"], role)
  end

  def is_dps(role) do
    Enum.member?(@dps, role) || Enum.member?(["melee", "physranged", "magicranged"], role)
  end

  def is_role(role) do
    Enum.member?(
      ["tank", "shhealer", "rghealer", "healer", "melee", "physranged", "magicranged"],
      role
    )
  end

  def get_role_name(role) do
    case role do
      "whitemage" -> "White Mage"
      "blackmage" -> "Black Mage"
      "redmage" -> "Red Mage"
      "rghealer" -> "Regen Healer"
      "shhealer" -> "Shield Healer"
      "physranged" -> "Phys. Ranged"
      "magicranged" -> "Magic Ranged"
      r -> String.capitalize(r)
    end
  end

  def get_role_color(role) do
    case get_role_type(role) do
      "tank" -> "#cfe2ff"
      "healer" -> "#d1e7dd"
      "dps" -> "#f8d7da"
      _ -> nil
    end
  end
end
