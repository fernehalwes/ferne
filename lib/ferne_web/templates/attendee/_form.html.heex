<.form let={f} for={@changeset} id="signup-form">
  <%= if Map.has_key?(@conn.query_params, "override_id") do %>
    <input type="hidden" name="override_id" id="override_id" value={@conn.query_params["override_id"]} />
  <% end %>
  <%= if Ferne.BlockDb.has_block?(@event.blocks, "discord_login") && @discord_username do %>
    <p>Your Discord username is <strong><%= @discord_username %></strong>.</p>
    <%= hidden_input f, :discord_id, value: @discord_id %>
    <%= hidden_input f, :discord_username, value: @discord_username %>
  <% end %>
  <%= if @event.store_name do %>
    <div class="mb-3">
      <%= label f, :name, "Name (what you'd like to be called)", class: "form-label" %>
      <%= text_input f, :name, class: "form-control" %>
      <%= error_tag_changeset @changeset, :name %>
    </div>
  <% end %>
  <div class="mb-3">
    <%= label f, :ign, "In-game name", class: "form-label form-label-required mb-0" %>
    <small class="text-muted d-block mb-2">No server names or anything of the sort needed.</small>
    <%= text_input f, :ign, class: "form-control", placeholder: "Example: First'name Last'name" %>
    <%= error_tag_changeset @changeset, :ign %>
  </div>
  <%= if @event.store_email do %>
    <div class="mb-3">
      <%= label f, :email, "Email (used only by the organizers to contact you)", class: "form-label" %>
      <%= email_input f, :email, class: "form-control" %>
      <%= error_tag_changeset @changeset, :email %>
    </div>
  <% end %>

  <%= for custom_field <- @custom_fields do %>
    <div class="mb-3">
      <%= if custom_field.type != "boolean" do %>
        <label class={"form-label #{if(Map.has_key?(custom_field, :required) && custom_field.required, do: "form-label-required")} mb-0"} for={"custom-field_#{custom_field.id}"}><%= custom_field.name %></label>
        <small class="text-muted d-block mb-2"><%= custom_field.description %></small>
      <% end %>
      <%= if custom_field.type == "string" do %>
        <input
          id={"custom-field_#{custom_field.id}"}
          class="form-control"
          name={"attendee[custom_fields][#{custom_field.id}]"}
          type="text"
          value={get_data_or_change(@changeset, custom_field.id)}
        />
      <% end %>
      <%= if custom_field.type == "boolean" do %>
        <div class="form-check">
          <input
            id={"custom-field_#{custom_field.id}"}
            class="form-check-input"
            name={"attendee[custom_fields][#{custom_field.id}]"}
            type="checkbox"
            checked={get_bool_data_or_change(@changeset, custom_field.id)}
          />
          <label class={"form-check-label #{if(Map.has_key?(custom_field, :required) && custom_field.required, do: "form-label-required")}"} for={"custom-field_#{custom_field.id}"}>
            <%= custom_field.name %>
            <small class="text-muted"><%= custom_field.description %></small>
          </label>
        </div>
      <% end %>
      <%= if custom_field.type == "single_select" do %>
        <%= for %{"id" => id, "value" => value} <- custom_field.choices do %>
          <div class="form-check">
            <input class="form-check-input" required={Map.has_key?(custom_field, :required) && custom_field.required} type="radio" name={"attendee[custom_fields][#{custom_field.id}]"} id={id} checked={get_sselect_data_or_change(@changeset, custom_field.id, id)} value={id} />
            <label class="form-check-label" for={id}><%= value %></label>
          </div>
        <% end %>
      <% end %>
      <%= if custom_field.type == "multi_select" do %>
        <%= for %{"id" => id, "value" => value} <- custom_field.choices do %>
          <div class="form-check">
            <input class="form-check-input" type="checkbox" required={Map.has_key?(custom_field, :required) && custom_field.required} name={"attendee[custom_fields][#{custom_field.id}][]"} id={id} checked={get_sselect_data_or_change(@changeset, custom_field.id, id)} value={id} />
            <label class="form-check-label" for={id}><%= value %></label>
          </div>
        <% end %>
      <% end %>
      <%= if custom_field.type == "textarea" do %>
        <textarea
          class="form-control"
          name={"attendee[custom_fields][#{custom_field.id}]"}>
          <%= get_data_or_change(@changeset, custom_field.id) %></textarea>
      <% end %>
      <%= error_tag_changeset @changeset, String.to_atom(custom_field.id) %>
    </div>
  <% end %>

  <%= if has_block?(@event.blocks, "ffxiv_roles") do %> 
    <div class="mb-3">
      <h5 class="mb-0"><%= block_data_or_default(find_block(@event.blocks, "ffxiv_roles"), "name", "Select your preferred classes") %> <span class="required-star">*</span></h5>
      <%= error_tag_changeset @changeset, :ffxiv_roles %>
      <small class="text-muted mb-3">
        <%= block_data_or_default(find_block(@event.blocks, "ffxiv_roles"), "description", "") %>
      </small>
      <% roles = find_block(@event.blocks, "ffxiv_roles") %>
      <div
        id="roles-mount"
        data-role-params={f.params["ffxiv_roles"]}
        data-existing-roles={@changeset.data.custom_data && @changeset.data.custom_data["ffxiv_roles"]}
        data-restrict-roles={roles.data["data"] && roles.data["data"]["restrict_roles"]}
        data-roles-only={roles.data["data"] && roles.data["data"]["roles_only"]}></div>
    </div>
  <% end %>

  <small class="d-block mb-2 text-muted"><span class="required-star">*</span> = required field</small>

  <%= submit @submit_text, class: "btn btn-primary" %>
  <%= if @event.attendee_limit do %>
    <small class="d-block mt-1 text-muted">Signing up does not guarantee participation in this event, as there is an attendee limit.</small>
  <% end %>
</.form>
