<%= render("_about.html", Map.merge(assigns, %{active: :members})) %>

<div class="mw-lg-xl m-auto">
  <%= if Bodyguard.permit?(Ferne.Team, :manage_team, @current_user, @team) do %>
    <div class="text-end">
      <%= link to: Routes.live_path(@conn, FerneWeb.AddTeamMembersLive, @team.id) do %>
        <button class="btn btn-primary">
          <i class="bi bi-person-plus-fill"></i> Invite 
        </button>
      <% end %>
    </div>
    <div class="text-muted mx-3 my-2">
      Psst: There's three team permission levels!
      The <strong>creator</strong> can do everything. Team <strong>admins</strong>
      can do everything the creator can do, with exception of deleting the team.
      Team <strong>organizers</strong> have <i>write</i> access to all events organized
      by the team. Team <strong>members</strong> have <i>read</i> access to all events
      organized by the team.
    </div>
  <% end %>
  <ul class="list-group list-group-flush">
    <%= for {member, tm} <- pair_team_members(@team) do %>
      <li class="list-group-item d-flex justify-content-between align-items-center">
        <div>
          <h5 class="fw-bold mb-0"><%= member.name %></h5>
          <div>@<%= member.username %></div>
        </div>
        <div>
          <%= if Bodyguard.permit?(Ferne.Team, :manage_team, @current_user, @team) do %>
            <%= if member.id == @team.user_id do %>
              Creator
            <% else %>
              <%= link if(tm.superadmin, do: "Remove Admin", else: "Make Admin"), to: Routes.team_path(@conn, :toggle_superadmin, @team.id, member.id), class: "btn btn-outline-primary" %>
              <%= link if(tm.admin, do: "Remove Organizer", else: "Make Organizer"), to: Routes.team_path(@conn, :toggle_admin, @team.id, member.id), class: "btn btn-outline-primary" %>
              <%= link "Remove", to: Routes.team_path(@conn, :remove_member, @team.id, member.id), class: "btn btn-danger" %>
            <% end %>
          <% else %>
            <%= cond do %>
              <% member.id == @team.user_id -> %>
                Creator
              <% tm.superadmin -> %>
                Admin
              <% tm.admin -> %>
                Organizer
              <% true -> %>
                Member
            <% end %>
          <% end %>
        </div>
      </li>
    <% end %>
  </ul>
</div>
