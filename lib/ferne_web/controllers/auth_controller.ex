defmodule FerneWeb.AuthController do
  use FerneWeb, :controller

  def start_discord(conn, %{"id" => event_id}) do
    client = get_discord_client(event_id)
    url = OAuth2.Client.authorize_url!(client, scope: "identify")

    conn
    |> redirect(external: url)
  end

  def finish_discord(conn, %{"code" => code, "state" => event_id}) do
    client = get_discord_client(event_id)
    client = OAuth2.Client.get_token!(client, code: code)
    user = OAuth2.Client.get!(client, "/users/@me")

    conn
    |> redirect(
      to:
        Routes.event_path(conn, :show_signup, event_id,
          did: user.body["id"] || "noid",
          username: user.body["username"],
          discriminator: user.body["discriminator"]
        )
    )
  end

  def finish_discord(conn, %{"error" => "access_denied", "state" => event_id} = _err) do
    conn
    |> put_flash(:error, "Unable to authorize with Discord. Did you click cancel on accident?")
    |> redirect(to: Routes.event_path(conn, :show_signup, event_id))
  end

  defp get_discord_client(event_id) do
    envs = Application.get_env(:ferne, :discord_auth)

    client =
      OAuth2.Client.new(
        strategy: OAuth2.Strategy.AuthCode,
        client_id: envs[:client_id],
        client_secret: envs[:client_secret],
        site: "https://discord.com/api",
        token_url: "/oauth2/token",
        authorize_url: "/oauth2/authorize",
        redirect_uri: envs[:redirect_url],
        params: %{state: event_id}
      )

    client = OAuth2.Client.put_serializer(client, "application/json", Jason)

    client
  end
end
