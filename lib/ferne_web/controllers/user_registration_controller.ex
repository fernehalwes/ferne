defmodule FerneWeb.UserRegistrationController do
  use FerneWeb, :controller

  alias Ferne.{User, UserToken, UserNotifier}

  def new(conn, _params) do
    changeset = User.registration_changeset(%User{}, %{})
    render(conn, "new.html", changeset: changeset, page_title: "Register")
  end

  def create(conn, %{"user" => user_params}) do
    case User.register(user_params) do
      {:ok, user} ->
        {encoded_token, user_token} = UserToken.build_email_token(user, "confirm")
        url = Routes.user_confirmation_url(conn, :edit, encoded_token)
        UserToken.create(user_token)
        UserNotifier.deliver_confirmation_instructions(user, url)

        conn
        |> put_flash(
          :info,
          "Signed up succesfully. Look for an email in your inbox to confirm your account!"
        )
        |> redirect(to: Routes.page_path(conn, :index))

      {:error, %Ecto.Changeset{} = changeset} ->
        render(conn, "new.html", changeset: changeset, page_title: "Register")
    end
  end
end
