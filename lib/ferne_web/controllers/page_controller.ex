defmodule FerneWeb.PageController do
  use FerneWeb, :controller
  alias Ferne.{Team, Event}

  def index(conn, _params) do
    if conn.assigns.current_user do
      events =
        Event.get_involved_for_user(conn.assigns.current_user)
        |> Ferne.Repo.preload([:team, :user])

      teams = Team.get_all_for_user(conn.assigns.current_user)

      render(conn, "dash.html", events: events, teams: teams, page_title: "Dashboard")
    else
      render(conn, "index.html")
    end
  end

  def frontpage(conn, _params) do
    render(conn, "index.html")
  end

  def about(conn, _params) do
    render(conn, "about.html")
  end

  def terms(conn, _params) do
    render(conn, "terms.html", page_title: "Terms of Service")
  end

  def privacy(conn, _params) do
    render(conn, "privacy.html", page_title: "Privacy Policy")
  end
end
