defmodule FerneWeb.UserSettingsController do
  use FerneWeb, :controller

  alias Ferne.{UserNotifier, User, UserToken}

  plug :assign_settings_changesets

  def edit(conn, _params) do
    render(conn, "edit.html", page_title: "Edit your user settings")
  end

  def update(conn, %{"action" => "update_profile"} = params) do
    %{"user" => user_params} = params
    user = conn.assigns.current_user

    case User.update_profile(user, user_params) do
      {:ok, changed_user} ->
        conn
        |> put_flash(:info, "Profile sucessfully updated!")
        |> render("edit.html", profile_changeset: User.profile_changeset(changed_user, %{}))

      {:error, changeset} ->
        render(conn, "edit.html", profile_changeset: changeset)
    end
  end

  def update(conn, %{"action" => "update_email"} = params) do
    %{"user" => user_params} = params
    user = conn.assigns.current_user

    case User.apply_email(user, user_params) do
      {:ok, applied_user} ->
        {encoded_token, user_token} =
          UserToken.build_email_token(applied_user, "change:#{user.email}")

        UserToken.create(user_token)
        url = Routes.user_settings_url(conn, :confirm_email, encoded_token)

        UserNotifier.deliver_update_email_instructions(applied_user, url)

        conn
        |> put_flash(
          :info,
          "A link to confirm your email change has been sent to the new address."
        )
        |> redirect(to: Routes.user_settings_path(conn, :edit))

      {:error, changeset} ->
        render(conn, "edit.html", email_changeset: changeset)
    end
  end

  def confirm_email(conn, %{"token" => token}) do
    case User.update_email(conn.assigns.current_user, token) do
      :ok ->
        conn
        |> put_flash(:info, "Email changed successfully.")
        |> redirect(to: Routes.user_settings_path(conn, :edit))

      :error ->
        conn
        |> put_flash(:error, "Email change link is invalid or it has expired.")
        |> redirect(to: Routes.user_settings_path(conn, :edit))
    end
  end

  def delete(conn, _params) do
    user = conn.assigns.current_user
    User.delete(user)

    conn
    |> put_flash(:info, "You've deleted your user account. Bye!")
    |> redirect(to: Routes.page_path(conn, :index))
  end

  defp assign_settings_changesets(conn, _opts) do
    user = conn.assigns.current_user

    conn
    |> assign(:email_changeset, User.email_changeset(user, %{}))
    |> assign(:profile_changeset, User.profile_changeset(user, %{}))
  end
end
