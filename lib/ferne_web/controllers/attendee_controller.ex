defmodule FerneWeb.AttendeeController do
  use FerneWeb, :controller
  alias Ferne.{BlockDb, Event, Attendee, AttentionList, Repo}

  def create(conn, %{"attendee" => attendee_params, "id" => id} = _params) do
    event =
      Event.get_by_id(id)
      |> Repo.preload([:user, :organizers, :team_organizers, :blocks, :team])

    lists =
      if event.team_id do
        AttentionList.by_team(%{id: event.team_id}) |> Repo.all()
      else
        nil
      end

    custom_data =
      put_custom_fields(event, attendee_params)
      |> put_discord_login(event, attendee_params)
      |> put_ffxiv_roles(event, attendee_params)

    {token, hashed_token} = Attendee.generate_hashed_token()

    changeset =
      Attendee.creation_changeset(%Attendee{}, attendee_params)
      |> Ecto.Changeset.change(%{
        event_id: event.id,
        custom_data: custom_data,
        edit_token: hashed_token
      })
      |> validate_required_custom_fields(event)
      |> validate_ffxiv_roles()

    with %{valid?: true} <- changeset,
         true <- Attendee.is_signup_unique?(changeset) do
      is_blacklisted? =
        lists &&
          Enum.filter(lists, &(&1.type == :black))
          |> Enum.map(&String.downcase(&1.ign))
          |> Enum.member?(String.downcase(attendee_params["ign"]))

      if !is_blacklisted? do
        Repo.insert(changeset)

        conn
        |> put_event_cookie(id)
        |> render("finish.html", token: token, event: event)
      else
        conn
        |> put_flash(:error, "Unable to sign up.")
        |> redirect(to: Routes.event_path(conn, :show_signup, event.id))
      end
    else
      %{errors: err} when is_list(err) and length(err) > 0 ->
        custom_fields =
          BlockDb.get_metadata_for_block([], event, "custom_fields") |> Enum.concat()

        conn
        |> put_flash(:error, "Invalid fields! Please check your entries.")
        |> put_view(FerneWeb.EventView)
        |> render("show_signup.html",
          event: event,
          changeset: changeset,
          custom_fields: custom_fields,
          page_title: event.name,
          discord_username: attendee_params["discord_username"],
          discord_id: attendee_params["discord_id"] || "noid"
        )

      false ->
        conn
        |> put_flash(:error, "You're already signed up to this event!")
        |> put_event_cookie(id)
        |> redirect(to: Routes.event_path(conn, :show_signup, event.id))
    end
  end

  def edit(conn, %{"token" => token} = params) do
    attendee =
      if params["override_id"] do
        attendee = Attendee.get_by_id(params["override_id"])

        event =
          Event.get_by_id(attendee.event_id)
          |> Repo.preload([:team, :organizers, :team_organizers])

        if Bodyguard.permit?(Event, :manage_event, conn.assigns.current_user, event) do
          attendee
        else
          nil
        end
      else
        {:ok, token} = Base.url_decode64(token, padding: false)

        Attendee.hash_token(token)
        |> Attendee.get_by_edit_token()
      end

    if attendee do
      event = Event.get_by_id(attendee.event_id) |> Ferne.Repo.preload(:blocks)
      changeset = Attendee.change_attendee(attendee, %{})

      custom_fields =
        BlockDb.get_metadata_for_block([], event, "custom_fields")
        |> Enum.concat()
        |> Enum.filter(&(!&1.editable))

      render(conn, "edit.html",
        event: event,
        changeset: changeset,
        custom_fields: custom_fields,
        page_title: "Edit information for #{event.name}"
      )
    else
      conn
      |> put_flash(:error, "The attendee you're trying to edit doesn't exist!")
      |> redirect(to: Routes.page_path(conn, :index))
    end
  end

  def update(conn, %{"token" => token, "attendee" => attendee_params} = params) do
    attendee =
      if params["override_id"] do
        attendee = Attendee.get_by_id(params["override_id"])

        event =
          Event.get_by_id(attendee.event_id)
          |> Repo.preload([:team, :organizers, :team_organizers])

        if Bodyguard.permit?(Event, :manage_event, conn.assigns.current_user, event) do
          attendee
        else
          nil
        end
      else
        {:ok, token} = Base.url_decode64(token, padding: false)

        Attendee.hash_token(token)
        |> Attendee.get_by_edit_token()
      end

    if attendee do
      event = Event.get_by_id(attendee.event_id) |> Repo.preload(:blocks)

      custom_data =
        put_custom_fields(event, attendee_params)
        |> put_ffxiv_roles(event, attendee_params)

      changeset =
        Attendee.creation_changeset(attendee, attendee_params)
        |> Ecto.Changeset.change(%{
          custom_data: Map.merge(attendee.custom_data, custom_data)
        })
        |> validate_required_custom_fields(event)
        |> validate_ffxiv_roles()

      if changeset.valid? do
        Repo.update(changeset)

        conn
        |> put_flash(:info, "Information updated!")
        |> redirect(
          to:
            if(params["override_id"],
              do: Routes.event_path(conn, :manage_attendees, event.id),
              else: Routes.attendee_path(conn, :edit, token)
            )
        )
      else
        custom_fields =
          BlockDb.get_metadata_for_block([], event, "custom_fields") |> Enum.concat()

        conn
        |> put_flash(:error, "Invalid fields! Please check your entries.")
        |> render("edit.html",
          event: event,
          changeset: changeset,
          custom_fields: custom_fields,
          page_title: event.name
        )
      end
    else
      conn
      |> put_flash(:error, "The attendee you're trying to edit doesn't exist!")
      |> redirect(to: Routes.page_path(conn, :index))
    end
  end

  def finish(conn, %{"id" => id} = _params) do
    event = Event.get_by_id(id)
    render(conn, "finish.html", event: event)
  end

  defp put_discord_login(map, event, params) do
    if BlockDb.has_block?(event.blocks, "discord_login") && params["discord_username"] &&
         params["discord_id"] do
      Map.put(map, "discord_login", %{
        username: params["discord_username"],
        id: params["discord_id"]
      })
    else
      map
    end
  end

  defp put_ffxiv_roles(map, event, params) do
    if BlockDb.has_block?(event.blocks, "ffxiv_roles") && params["ffxiv_roles"] do
      Map.put(map, "ffxiv_roles", params["ffxiv_roles"])
    else
      map
    end
  end

  defp put_custom_fields(event, params) do
    if BlockDb.has_block?(event.blocks, "custom_fields") && params["custom_fields"] do
      Map.new(params["custom_fields"], fn {k, v} ->
        val =
          case v do
            "on" ->
              "Yes"

            "off" ->
              "No"

            _ ->
              v
          end

        {k, val}
      end)
    else
      %{}
    end
  end

  defp validate_ffxiv_roles(changeset) do
    if Map.has_key?(changeset.changes, :custom_data) &&
         changeset.changes.custom_data["ffxiv_roles"] &&
         changeset.changes.custom_data["ffxiv_roles"] == "" do
      changeset
      |> Ecto.Changeset.add_error(:ffxiv_roles, "is required")
    else
      changeset
    end
  end

  defp validate_required_custom_fields(changeset, event) do
    block = BlockDb.find_block(event.blocks, "custom_fields")

    if block && Map.has_key?(changeset.changes, :custom_data) && block.data["data"] do
      required_fields = Enum.filter(block.data["data"], & &1["required"])

      Enum.reduce(required_fields, changeset, fn data, acc ->
        # FIXME: horrible brain fart control flow
        if changeset.data.custom_data[data["id"]] == "" &&
             changeset.changes.custom_data[data["id"]] == "" do
          Ecto.Changeset.add_error(acc, String.to_atom(data["id"]), "is required")
        else
          if changeset.changes.custom_data[data["id"]] == "" do
            Ecto.Changeset.add_error(acc, String.to_atom(data["id"]), "is required")
          else
            acc
          end
        end
      end)
    else
      changeset
    end
  end

  defp put_event_cookie(conn, id) do
    with curr_events when not is_nil(curr_events) <- conn.req_cookies()["_ferne_current_events"],
         {:ok, list} <- Jason.decode(curr_events),
         nil <- Enum.find(list, &(&1 == id)) do
      # Cookie exists, event is not in it: Store the event in it
      put_resp_cookie(conn, "_ferne_current_events", Jason.encode!([id | list]), http_only: false)
    else
      value when is_binary(value) ->
        # Cookie exists, event is in it: Do nothing
        conn

      {:error, _} ->
        # Cookie exists, JSON decode error: Remove the cookie
        delete_resp_cookie(conn, "_ferne_current_events")

      nil ->
        # Cookie does not exist: Store cookie with event
        put_resp_cookie(conn, "_ferne_current_events", Jason.encode!([id]), http_only: false)
    end
  end
end
