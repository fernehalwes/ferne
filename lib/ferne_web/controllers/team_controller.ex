defmodule FerneWeb.TeamController do
  @moduledoc """
  Controller for team routes.
  """
  use FerneWeb, :controller
  alias Ferne.{Team, Event, Repo, EventTemplate, TeamInvite, TeamMember}

  plug :authorize_manage_team
       when action in [
              :edit,
              :update,
              :delete,
              :toggle_admin,
              :toggle_superadmin,
              :remove_member,
              :delete
            ]

  def new(conn, _params) do
    render(conn, "new.html",
      changeset: Team.creation_changeset(%Team{}),
      page_title: "Create new team"
    )
  end

  def create(conn, %{"team" => params}) do
    case Team.create(params, conn.assigns.current_user) do
      {:ok, team} ->
        conn
        |> put_flash(:info, "Your team has been created successfully!")
        |> redirect(to: Routes.team_path(conn, :show, team.id))

      {:error, %Ecto.Changeset{} = changeset} ->
        conn
        |> put_flash(:error, "Error while creating team! Please check your inputs.")
        |> render("new.html",
          changeset: changeset,
          page_title: "Create new team"
        )
    end
  end

  def edit(conn, _params) do
    render(conn, "edit.html",
      changeset: Team.creation_changeset(conn.assigns.team),
      page_title: "Edit #{conn.assigns.team.name}"
    )
  end

  def update(conn, %{"team" => params}) do
    if Bodyguard.permit?(Team, :manage_team, conn.assigns.current_user, conn.assigns.team) do
      case Team.update(conn.assigns.team, params) do
        {:ok, team} ->
          conn
          |> put_flash(:info, "Team details updated successfully!")
          |> redirect(to: Routes.team_path(conn, :show, team.id))
  
        {:error, %Ecto.Changeset{} = changeset} ->
          conn
          |> put_flash(:error, "Error while updating team details! Please check your inputs.")
          |> render("edit.html",
            changeset: changeset,
            page_title: "Edit #{conn.assigns.team.name}"
          )
      end
    end
  end

  def show(conn, %{"id" => id}) do
    team = Team.get_by_id(id) |> Ferne.Repo.preload([:members, :team_members])

    if team do
      events =
        if Bodyguard.permit?(Team, :access_team, conn.assigns.current_user, team),
          do: Event.by_team(team) |> Repo.all(),
          else: Event.by_team(team) |> Event.by_public() |> Repo.all()

      render(conn, "show.html", team: team, public_events: events, page_title: team.name)
    else
      conn
      |> put_view(FerneWeb.ErrorView)
      |> render("404.html")
    end
  end

  def show_members(conn, %{"id" => id}) do
    team = Team.get_by_id(id) |> Ferne.Repo.preload([:members, :team_members])

    if team.public_members ||
         Bodyguard.permit?(Team, :access_team, conn.assigns.current_user, team) do
      render(conn, "show_members.html", team: team, page_title: "Members of #{team.name}")
    else
      conn
      |> put_flash(:error, "You're not authorized to do that.")
      |> redirect(to: Routes.team_path(conn, :show, id))
    end
  end

  def show_templates(conn, %{"id" => id}) do
    team = Team.get_by_id(id) |> Ferne.Repo.preload([:members, :team_members])
    templates = EventTemplate.by_team(team) |> Ferne.Repo.all()

    render(conn, "show_templates.html",
      templates: templates,
      team: team,
      page_title: "Templates of #{team.name}"
    )
  end

  def accept_invite(conn, %{"token" => token}) do
    case TeamInvite.confirm(token, conn.assigns.current_user) do
      {:ok, team} ->
        conn
        |> put_flash(:info, "You've been added to #{team.name}!")
        |> redirect(to: Routes.team_path(conn, :show, team.id))

      :error ->
        conn
        |> put_flash(:error, "Something went wrong when trying to accept this invite.")
        |> redirect(to: Routes.page_path(conn, :index))
    end
  end

  def toggle_admin(conn, %{"user_id" => user_id}) do
    tm = TeamMember.get_for_team_and_user(conn.assigns.team, %{id: user_id})

    if Bodyguard.permit?(Team, :manage_team, conn.assigns.current_user, conn.assigns.team) do
      changeset = TeamMember.edit_admin_changeset(tm, %{admin: !tm.admin})
      Ferne.Repo.update(changeset)
    end

    conn
    |> redirect(to: Routes.team_path(conn, :show_members, conn.assigns.team.id))
  end

  def toggle_superadmin(conn, %{"user_id" => user_id}) do
    tm = TeamMember.get_for_team_and_user(conn.assigns.team, %{id: user_id})

    if Bodyguard.permit?(Team, :manage_team, conn.assigns.current_user, conn.assigns.team) do
      changeset = TeamMember.edit_superadmin_changeset(tm, %{superadmin: !tm.superadmin})
      Ferne.Repo.update(changeset)
    end

    conn
    |> redirect(to: Routes.team_path(conn, :show_members, conn.assigns.team.id))
  end

  def remove_member(conn, %{"user_id" => user_id}) do
    tm = TeamMember.get_for_team_and_user(conn.assigns.team, %{id: user_id})

    if Bodyguard.permit?(Team, :manage_team, conn.assigns.current_user, conn.assigns.team) do
      Ferne.Repo.delete(tm)
    end

    conn
    |> put_flash(:info, "Team member removed.")
    |> redirect(to: Routes.team_path(conn, :show_members, conn.assigns.team.id))
  end

  def delete(conn, _params) do
    if Bodyguard.permit?(Team, :delete_team, conn.assigns.current_user, conn.assigns.team) do
      Team.delete(conn.assigns.team)
    end

    conn
    |> redirect(to: Routes.page_path(conn, :index))
  end

  def authorize_manage_team(conn, _opts) do
    team = Team.get_by_id(conn.path_params["id"]) |> Repo.preload([:team_members])

    if Bodyguard.permit?(Team, :manage_team, conn.assigns.current_user, team) do
      conn
      |> assign(:team, team)
    else
      conn
      |> put_flash(:error, "You're not authorized to do that.")
      |> redirect(to: Routes.page_path(conn, :index))
    end
  end
end
