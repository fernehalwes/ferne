defmodule FerneWeb.AttentionListController do
  @moduledoc """
  Controller for attendee lists.
  """
  use FerneWeb, :controller
  alias Ferne.{AttentionList, Team, Repo}
  alias Ecto.Multi

  def show(conn, %{"id" => id} = params) do
    team = Team.get_by_id(id) |> Repo.preload([:team_members, :members])

    lists =
      AttentionList.get_all_by_team_id(id)
      |> Repo.preload([:author])
      |> Enum.group_by(& &1.type)

    render(conn, "show.html", team: team, lists: lists, type: params["type"] || "favor")
  end

  def new(conn, %{"id" => id} = params) do
    team = Team.get_by_id(id) |> Repo.preload([:team_members])

    if Bodyguard.permit?(Ferne.Team, :administrate_team, conn.assigns.current_user, team) do
      render(conn, "new.html",
        team: team,
        type: params["type"] || "favor",
        changeset: AttentionList.insert_changeset(%AttentionList{})
      )
    else
      conn
      |> put_flash(:error, "You're not authorized to do this.")
      |> redirect(to: Routes.page_path(conn, :index))
    end
  end

  def create(conn, %{"id" => id, "attention_list" => params}) do
    team = Team.get_by_id(id)
    prelim_changeset = AttentionList.insert_changeset(%AttentionList{}, params)

    if params["ign"] && !AttentionList.validate_bulk_structure(params["ign"]) do
      conn
      |> put_flash(:error, "Error when submitting! Check the errors below.")
      |> render("new.html",
        team: team,
        type: params["type"],
        changeset:
          Map.put(prelim_changeset, :action, :create)
          |> Ecto.Changeset.add_error(:ign, "wrong format!")
      )
    else
      results = AttentionList.extract_bulk_structure(params["ign"])
      multi = Multi.new()

      Enum.reduce(results, multi, fn r, acc ->
        split = String.split(r, ",")
        ign = Enum.at(split, 0) |> String.trim()
        reason = if(Enum.at(split, 1), do: String.trim(Enum.at(split, 1)), else: nil)

        Multi.insert(
          acc,
          String.to_atom(ign),
          AttentionList.insert_changeset(%AttentionList{}, %{
            ign: ign,
            reason: reason,
            type: params["type"],
            inserted_by: conn.assigns.current_user.id,
            team_id: team.id
          })
        )
      end)
      |> Repo.transaction()

      conn
      |> put_flash(:info, "Entries successfully added!")
      |> redirect(to: Routes.attention_list_path(conn, :show, team.id, type: params["type"]))
    end
  end

  def delete(conn, %{"id" => id} = params) do
    entry = Repo.get(AttentionList, id)
    team = Repo.get(Ferne.Team, entry.team_id) |> Repo.preload([:team_members])

    if Bodyguard.permit?(Ferne.Team, :administrate_team, conn.assigns.current_user, team) do
      Repo.delete(entry)

      conn
      |> put_flash(:info, "Entry deleted successfully!")
      |> redirect(
        to:
          Routes.attention_list_path(conn, :show, entry.team_id, type: params["type"] || "favor")
      )
    end
  end
end
