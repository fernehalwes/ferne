defmodule FerneWeb.UserSessionController do
  use FerneWeb, :controller

  alias Ferne.{User, UserToken, UserNotifier}
  alias FerneWeb.UserAuth

  def new(conn, _params) do
    render(conn, "new.html", error_message: nil, page_title: "Log in")
  end

  def create(conn, %{"user" => user_params}) do
    %{"email" => email} = user_params

    case User.get_by_email(email) do
      %{confirmed_at: c} = user when not is_nil(c) ->
        UserAuth.send_login_token_for_user(conn, user)

      %{confirmed_at: nil} = user ->
        if user_params["resend_confirmation"] && user_params["resend_confirmation"] == "on" do
          {encoded_token, user_token} = UserToken.build_email_token(user, "confirm")
          url = Routes.user_confirmation_path(conn, :edit, encoded_token)
          UserToken.create(user_token)
          UserNotifier.deliver_confirmation_instructions(user, url)
        end

      nil ->
        true
    end

    # Render the send template regardless of whether the email
    # exists or not, in order to prevent user enumeration attacks.
    conn
    |> render("sent.html", page_title: "Check your email!")
  end

  def login(conn, %{"token" => token}) do
    render(conn, "login.html", token: token)
  end

  def post_login(conn, %{"token" => token}) do
    case UserToken.verify_login_token(token) do
      {:ok, user} ->
        UserAuth.log_in_user(conn, user)

      :error ->
        conn
        |> put_flash(:error, "Error when logging in!")
        |> redirect(to: Routes.page_path(conn, :index))
    end
  end

  def delete(conn, _params) do
    conn
    |> put_flash(:info, "Logged out successfully.")
    |> UserAuth.log_out_user()
  end
end
