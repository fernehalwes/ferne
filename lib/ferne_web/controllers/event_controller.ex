defmodule FerneWeb.EventController do
  use FerneWeb, :controller
  alias Ferne.{Attendee, EventTemplate, Event, Team, BlockDb}
  alias Elixlsx.{Sheet, Workbook}

  plug :authorize_view_organizer
       when action in [
              :show_manage,
              :block_special_get,
              :manage_attendees
            ]

  plug :authorize_manage_organizer
       when action in [
              :activate_block,
              :block_post,
              :block_get,
              :block_delete,
              :toggle_registrations,
              :toggle_public,
              :new_template,
              :create_template,
              :export_sheet,
              :export_party_sheet
            ]

  def show_manage(conn, %{"id" => _}) do
    event = conn.assigns.event
    blocks = BlockDb.get_blocks_for_event(event)
    unused_blocks = BlockDb.get_unused_blocks_for_event(blocks)
    teams_with_admin = Team.get_all_for_user_with_admin(conn.assigns.current_user)

    render(conn, "show_manage.html",
      event: event,
      blocks: blocks,
      unused_blocks: unused_blocks,
      teams_with_admin: teams_with_admin,
      page_title: "Manage #{event.name}"
    )
  end

  def manage_attendees(conn, %{"id" => _}) do
    event = conn.assigns.event

    render(conn, "manage_attendees.html",
      event: event,
      omit_container: true,
      page_title: "Manage Attendees"
    )
  end

  def activate_block(conn, %{"id" => _, "block" => block}) do
    event = conn.assigns.event

    if Enum.find(event.blocks, fn b -> b.identifier === block end) do
      conn
      |> put_flash(:error, "You've already added this block!")
      |> redirect(to: Routes.event_path(conn, :show_manage, event.id))
    else
      BlockDb.create_block(%{identifier: block}, event)

      conn
      |> redirect(to: Routes.event_path(conn, :show_manage, event.id))
    end
  end

  def block_get(conn, %{"id" => _, "block" => block}) do
    event = conn.assigns.event

    if Enum.find(event.blocks, fn b -> b.identifier == block end) do
      BlockDb.block_controller_get(conn, block, event)
    end
  end

  def block_post(conn, %{"id" => _, "block" => block} = params) do
    event = conn.assigns.event

    if Enum.find(event.blocks, fn b -> b.identifier == block end) do
      BlockDb.block_controller_post(conn, block, event, params)
    end
  end

  def block_special_get(conn, %{"id" => _, "block" => block}) do
    event = conn.assigns.event

    if BlockDb.has_block?(event.blocks, block) do
      BlockDb.block_controller_special_get(conn, block, event)
    end
  end

  def block_delete(conn, %{"id" => _, "block" => block}) do
    event = conn.assigns.event

    potential_block = Enum.find(event.blocks, &(&1.identifier === block))

    if potential_block do
      case BlockDb.delete_block(potential_block) do
        {:ok, _} ->
          conn
          |> redirect(to: Routes.event_path(conn, :show_manage, event.id))

        {:error, _} ->
          conn
          |> put_flash(:error, "Server error when trying to delete block!")
      end
    end
  end

  def show_signup(conn, %{"id" => id} = params) do
    event =
      Event.get_by_id(id)
      |> Ferne.Repo.preload([:blocks, :user, :organizers, :team_organizers, :team])

    changeset = Attendee.change_attendee(%Attendee{}, %{})

    custom_fields =
      BlockDb.get_metadata_for_block([], event, "custom_fields")
      |> Enum.concat()
      |> Enum.filter(&(!&1.editable))

    discord_username =
      if params["username"] && params["discriminator"] do
        "#{params["username"]}##{params["discriminator"]}"
      end

    discord_id = params["did"] || "noid"

    # TODO: This is a very primitive excerpt algorithm
    meta_description =
      cond do
        is_nil(event.description) -> "An event hosted on Fernehalwes."
        String.length(event.description) < 280 -> event.description
        true -> String.slice(event.description, 0..280) <> "..."
      end

    render(conn, "show_signup.html",
      event: event,
      changeset: changeset,
      custom_fields: custom_fields,
      page_title: event.name,
      page_description: meta_description,
      discord_username: discord_username,
      discord_id: discord_id
    )
  end

  def toggle_registrations(conn, %{"id" => _}) do
    event = conn.assigns.event

    Event.state_changeset(
      event,
      if(event.state == "closed", do: "planned", else: "closed")
    )
    |> Ferne.Repo.update()

    conn
    |> put_flash(
      :info,
      "You've #{if(event.state == "closed", do: "reopened", else: "closed")} registrations for this event."
    )
    |> redirect(to: Routes.event_path(conn, :show_manage, event.id))
  end

  def toggle_public(conn, %{"id" => _}) do
    event = conn.assigns.event

    Event.creation_changeset(event, %{public: !event.public})
    |> Ferne.Repo.update()

    conn
    |> put_flash(:info, "Changed public status for event successfully.")
    |> redirect(to: Routes.event_path(conn, :show_manage, event.id))
  end

  def cancel(conn, %{"id" => id}) do
    event = Event.get_by_id(id)

    if Bodyguard.permit?(Event, :mutate_event, conn.assigns.current_user, event) do
      Event.state_changeset(event, "cancelled")
      |> Ferne.Repo.update()

      conn
      |> put_flash(:info, "Successfully cancelled event!")
      |> redirect(to: Routes.event_path(conn, :show_manage, event.id))
    else
      conn
      |> put_flash(:error, "You're not authorized for this.")
      |> redirect(to: Routes.page_path(conn, :index))
    end
  end

  def delete(conn, %{"id" => id}) do
    event = Event.get_by_id(id)

    if Bodyguard.permit?(Event, :mutate_event, conn.assigns.current_user, event) do
      {:ok, _} = Ferne.Repo.delete(event)

      conn
      |> put_flash(:info, "Sucessfully deleted event!")
      |> redirect(to: Routes.page_path(conn, :index))
    else
      conn
      |> put_flash(:error, "You're not authorized for this.")
      |> redirect(to: Routes.page_path(conn, :index))
    end
  end

  def new_template(conn, _opts) do
    teams = Team.get_all_for_user_with_admin(conn.assigns.current_user)

    if length(teams) > 0 do
      render(conn, "new_template.html",
        teams: teams,
        page_title: "Save event as template",
        changeset: EventTemplate.creation_changeset(%EventTemplate{})
      )
    else
      conn
      |> put_flash(:error, "You have no teams that the template could be created in!")
      |> redirect(to: Routes.event_path(conn, :show_manage, conn.assigns.event.id))
    end
  end

  def create_template(conn, %{"event_template" => attrs}) do
    case EventTemplate.create(attrs, conn.assigns.event) do
      {:ok, template} ->
        conn
        |> put_flash(:info, "Template created successfully.")
        |> redirect(to: Routes.team_path(conn, :show, template.team_id))

      {:error, changeset} ->
        teams = Team.get_all_for_user_with_admin(conn.assigns.current_user)

        conn
        |> put_flash(:error, "Error while creating event template. Check your inputs.")
        |> render("new_template.html",
          teams: teams,
          page_title: "Save event as template",
          changeset: changeset
        )
    end
  end

  def new_via_template(conn, %{"id" => id}) do
    template = EventTemplate.get_by_id(id)
    team = Team.get_by_id(template.team_id) |> Ferne.Repo.preload([:members])

    if Bodyguard.permit?(Team, :access_team, conn.assigns.current_user, team) do
      render(conn, "new_via_template.html", template: template)
    else
      conn
      |> put_flash(:error, "You're not authorized for this.")
      |> redirect(to: Routes.page_path(conn, :index))
    end
  end

  def create_via_template(conn, %{"event" => event_params, "id" => id}) do
    template = EventTemplate.get_by_id(id)

    case EventTemplate.create_event_from_template(
           template,
           conn.assigns.current_user,
           event_params
         ) do
      {:ok, event} ->
        conn
        |> put_flash(:info, "Created event successfully!")
        |> redirect(to: Routes.event_path(conn, :show_manage, event.id))

      {:error, _} ->
        conn
        |> put_flash(:error, "Error while creating event!")
        |> redirect(to: Routes.event_path(conn, :new_via_template, template.id))
    end
  end

  def delete_template(conn, %{"id" => template_id}) do
    template = EventTemplate.get_by_id(template_id)
    team = Team.get_by_id(template.team_id) |> Ferne.Repo.preload([:team_members])

    if Bodyguard.permit?(Team, :administrate_team, conn.assigns.current_user, team) do
      {:ok, _} = EventTemplate.delete(template)

      conn
      |> put_flash(:info, "Template successfully deleted!")
      |> redirect(to: Routes.team_path(conn, :show_templates, template.team_id))
    else
      conn
      |> put_flash(:error, "You're not authorized to do that.")
      |> redirect(to: Routes.page_path(conn, :index))
    end
  end

  def export_sheet(conn, _params) do
    event = conn.assigns.event
    headers = BlockDb.get_metadata_for_event(event)

    body =
      headers
      |> Map.new(fn header ->
        {header.id,
         Map.new(event.attendees, fn %{custom_data: data} = att ->
           {att.id, if(data[header.id], do: data[header.id], else: nil)}
         end)}
      end)

    sheet = %Sheet{
      name: "Event Attendees",
      rows:
        [
          [
            ["IGN", bold: true]
          ]
          |> Enum.concat(if(event.store_name, do: [["Name", bold: true]], else: []))
          |> Enum.concat(
            Enum.map(
              headers,
              &[if(Map.has_key?(&1, :short_name), do: &1.short_name, else: &1.name), bold: true]
            )
          )
          |> Enum.concat([["Signed up at", bold: true]])
        ]
        |> Enum.concat(
          Enum.map(event.attendees, fn att ->
            [
              att.ign
            ]
            |> Enum.concat(if(event.store_name, do: [att.name], else: []))
            |> Enum.concat(
              Enum.map(
                headers,
                &(FerneWeb.AttendeeView.display_field_value(&1, att, body) ||
                    :empty)
              )
            )
            |> Enum.concat([[DateTime.to_unix(att.inserted_at), datetime: true]])
          end)
        )
    }

    {:ok, {_, binary}} = Elixlsx.write_to_memory(%Workbook{sheets: [sheet]}, "sheet")

    conn
    |> send_download({:binary, binary}, filename: "#{Slug.slugify(event.name)}.xlsx")
    |> redirect(to: Routes.event_path(conn, :manage_attendees, event.id))
  end

  def export_party_sheet(conn, _params) do
    event = conn.assigns.event
    parties_block = BlockDb.find_block(event.blocks, "parties")

    parties =
      if(parties_block.data["data"],
        do:
          parties_block.data["data"]
          |> Enum.map(&Ecto.Changeset.apply_changes(Ferne.BlockDb.Parties.change(&1))),
        else: []
      )

    groups = BlockDb.Parties.group_attendees_by_party(event.attendees, parties)

    longest =
      Enum.max_by(Map.values(groups) |> Enum.reject(&is_nil/1), fn g -> length(g) end) |> length()

    sheet = %Sheet{
      name: "Event Parties",
      rows:
        [
          Enum.map(parties, &[&1.name, bold: true])
        ]
        |> Enum.concat(
          Enum.map(0..(longest - 1), fn i ->
            Enum.map(parties, fn party ->
              if Map.has_key?(groups, party.id) do
                att = Enum.at(groups[party.id], i)

                bgcolor =
                  if(att && BlockDb.has_block?(event.blocks, "ffxiv_roles"),
                    do: FerneWeb.Roles.get_role_color(att.custom_data["party"]["role"]),
                    else: nil
                  )

                if(att,
                  do: [att.ign, bold: att.custom_data["party"]["leader"], bg_color: bgcolor],
                  else: :empty
                )
              else
                :empty
              end
            end)
          end)
        )
    }

    {:ok, {_, binary}} = Elixlsx.write_to_memory(%Workbook{sheets: [sheet]}, "sheet")

    conn
    |> send_download({:binary, binary}, filename: "#{Slug.slugify(event.name)}-parties.xlsx")
    |> redirect(to: Routes.event_path(conn, :manage_attendees, event.id))
  end

  def authorize_view_organizer(conn, _opts) do
    event =
      Event.get_by_id(conn.path_params["id"])
      |> Ferne.Repo.preload([:user, :attendees, :organizers, :blocks, :team, :team_organizers])

    if Bodyguard.permit?(Event, :view_manage_event, conn.assigns.current_user, event) do
      conn
      |> assign(:event, event)
    else
      conn
      |> put_flash(:error, "You're not authorized to do that.")
      |> redirect(to: Routes.page_path(conn, :index))
    end
  end

  def authorize_manage_organizer(conn, _opts) do
    event =
      Event.get_by_id(conn.path_params["id"])
      |> Ferne.Repo.preload([:user, :attendees, :organizers, :blocks, :team, :team_organizers])

    if Bodyguard.permit?(Event, :manage_event, conn.assigns.current_user, event) do
      conn
      |> assign(:event, event)
    else
      conn
      |> put_flash(:error, "You're not authorized to do that.")
      |> redirect(to: Routes.page_path(conn, :index))
    end
  end
end
