defmodule FerneWeb.ManageEventOrganizersLive do
  @moduledoc """
  Live view component for rendering the "manage event organizers" route.
  """
  use FerneWeb, :live_view
  alias Ferne.{Repo, User, Event, EventOrganizer}

  def render(assigns) do
    render(FerneWeb.EventView, "manage_event_organizers.live.html", assigns)
  end

  def mount(%{"id" => id}, %{"user_token" => user_token} = _session, socket) do
    event =
      Event.get_by_id(id)
      |> Repo.preload([:user, :organizers])

    socket =
      socket
      |> assign(user_token: user_token)

    user = FerneWeb.UserAuth.fetch_live_user(socket)

    if Bodyguard.permit?(Event, :mutate_event, user, event) do
      {:ok,
       socket
       |> assign(
         event: event,
         search_results: [],
         current_user: user,
         page_title: "Manage organizers for #{event.name}"
       )}
    else
      {:ok,
       socket
       |> put_flash(:error, "You're not authorized for this.")
       |> redirect(to: Routes.page_path(socket, :index))}
    end
  end

  def handle_event("search", %{"name" => %{"name" => ""}}, socket) do
    {:noreply, assign(socket, search_results: [])}
  end

  def handle_event("search", %{"name" => params}, socket) do
    # Avoid LIKE injections
    sanitized_username = String.replace(params["name"], "%", " ")

    matches =
      User.search_by_username(sanitized_username)
      |> Enum.filter(fn f ->
        !Enum.any?(socket.assigns.event.event_organizers, &(&1.user_id == f.id)) &&
          f.id != socket.assigns.current_user.id
      end)

    {:noreply, assign(socket, search_results: matches)}
  end

  def handle_event("addOrganizer", %{"organizer" => params}, socket) do
    user = socket.assigns.search_results |> Enum.find(&(&1.id == params["id"]))

    EventOrganizer.create(
      %{role: params["role"]},
      user,
      socket.assigns.event
    )

    event = Event.get_by_id(socket.assigns.event.id) |> Repo.preload(:organizers)
    {:noreply, assign(socket, search_results: [], event: event)}
  end

  def handle_event("removeOrganizer", %{"id" => id} = _params, socket) do
    organizer = Enum.find(socket.assigns.event.organizers, &(&1.id == id))
    EventOrganizer.delete(organizer, socket.assigns.event)

    event = Event.get_by_id(socket.assigns.event.id) |> Repo.preload(:organizers)
    {:noreply, assign(socket, search_results: [], event: event)}
  end
end
