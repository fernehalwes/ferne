defmodule FerneWeb.CustomFieldsLive do
  @moduledoc """
  Live component for editing custom fields.
  """
  use FerneWeb, :live_view
  alias Ferne.{BlockDb, Block, Repo}

  def render(assigns) do
    render(FerneWeb.BlockView, "custom_fields.live.html", assigns)
  end

  def mount(_params, %{"event" => event} = _session, socket) do
    block = BlockDb.get_block_for_event("custom_fields", event)
    fields = block.data["data"]

    {:ok,
     assign(socket, event: event, block: block, fields: fields, page_title: "Edit Custom Fields")}
  end

  def handle_event("add_field", _values, socket) do
    new_field = %{
      "id" => Ecto.UUID.generate(),
      "name" => "New Field",
      "short_name" => "New Field",
      "type" => "string",
      "has_form" => false,
      "required" => false,
      "description" => nil
    }

    {:noreply,
     assign(socket,
       fields:
         if(!socket.assigns.fields || Enum.empty?(socket.assigns.fields),
           do: [new_field],
           else: socket.assigns.fields ++ [new_field]
         )
     )}
  end

  def handle_event("change_field_attr", %{"id" => id, "attr" => attr, "value" => val}, socket) do
    {idx, field} = get_field_and_idx(socket.assigns.fields, id)

    {:noreply,
     assign(socket,
       fields: List.replace_at(socket.assigns.fields, idx, %{field | attr => val})
     )}
  end

  def handle_event("toggle_has_form", %{"id" => id}, socket) do
    {idx, field} = get_field_and_idx(socket.assigns.fields, id)

    {:noreply,
     assign(socket,
       fields:
         List.replace_at(socket.assigns.fields, idx, %{field | "has_form" => !field["has_form"]})
     )}
  end

  def handle_event("remove_field", %{"id" => id}, socket) do
    {idx, _field} = get_field_and_idx(socket.assigns.fields, id)

    {:noreply,
     socket
     |> assign(fields: List.delete_at(socket.assigns.fields, idx))}
  end

  def handle_event("move_field_up", %{"id" => id}, socket) do
    {idx, _field} = get_field_and_idx(socket.assigns.fields, id)

    if idx > 0 && idx < length(socket.assigns.fields) do
      new_idx = idx - 1

      {:noreply, assign(socket, fields: swap_els(socket.assigns.fields, idx, new_idx))}
    else
      {:noreply, socket}
    end
  end

  def handle_event("move_field_down", %{"id" => id}, socket) do
    {idx, _field} = get_field_and_idx(socket.assigns.fields, id)

    if idx < length(socket.assigns.fields) - 1 do
      new_idx = idx + 1

      {:noreply, assign(socket, fields: swap_els(socket.assigns.fields, idx, new_idx))}
    else
      {:noreply, socket}
    end
  end

  def handle_event("add_select_choice", %{"id" => id, "value" => value}, socket) do
    if String.length(value) == 0 do
      {:noreply, socket}
    else
      {idx, field} = get_field_and_idx(socket.assigns.fields, id)
      choices = if field["choices"], do: field["choices"], else: []
      new_id = Ecto.UUID.generate()

      {:noreply,
       socket
       |> assign(
         fields:
           List.replace_at(
             socket.assigns.fields,
             idx,
             Map.put(field, "choices", choices ++ [%{"id" => new_id, "value" => value}])
           )
       )
       |> push_event("clearInput", %{id: id})}
    end
  end

  def handle_event("delete_select_choice", %{"field-id" => id, "choice-id" => choice}, socket) do
    {idx, field} = get_field_and_idx(socket.assigns.fields, id)

    field_idx =
      if field["choices"],
        do: Enum.find_index(field["choices"], fn %{"id" => id} -> id === choice end)

    {:noreply,
     assign(socket,
       fields:
         List.replace_at(socket.assigns.fields, idx, %{
           field
           | "choices" => List.delete_at(field["choices"], field_idx)
         })
     )}
  end

  def handle_event("move_select_up", %{"field-id" => id, "choice-id" => choice}, socket) do
    {idx, field} = get_field_and_idx(socket.assigns.fields, id)
    old_idx = Enum.find_index(field["choices"], fn %{"id" => id} -> id == choice end)

    if old_idx > 0 && old_idx < length(field["choices"]) do
      new_idx = old_idx - 1

      new_choices =
        field["choices"]
        |> swap_els(old_idx, new_idx)

      {:noreply,
       assign(socket,
         fields: List.replace_at(socket.assigns.fields, idx, %{field | "choices" => new_choices})
       )}
    else
      {:noreply, socket}
    end
  end

  def handle_event("move_select_down", %{"field-id" => id, "choice-id" => choice}, socket) do
    {idx, field} = get_field_and_idx(socket.assigns.fields, id)
    old_idx = Enum.find_index(field["choices"], fn %{"id" => id} -> id == choice end)

    if old_idx >= 0 && old_idx < length(field["choices"]) - 1 do
      new_idx = old_idx + 1

      new_choices =
        field["choices"]
        |> swap_els(old_idx, new_idx)

      {:noreply,
       assign(socket,
         fields: List.replace_at(socket.assigns.fields, idx, %{field | "choices" => new_choices})
       )}
    else
      {:noreply, socket}
    end
  end

  def handle_event("save", _values, socket) do
    socket.assigns.block
    |> Block.change_block(%{data: %{data: socket.assigns.fields}})
    |> Repo.update!()

    {:noreply,
     socket
     |> push_event("showToast", %{text: "Changes saved!"})}
  end

  def handle_event("toggle_required", %{"id" => id}, socket) do
    {idx, field} = get_field_and_idx(socket.assigns.fields, id)

    {:noreply,
     assign(socket,
       fields:
         List.replace_at(
           socket.assigns.fields,
           idx,
           Map.put(field, "required", !Map.has_key?(field, "required") || !field["required"])
         )
     )}
  end

  defp get_field_and_idx(fields, id) do
    idx = Enum.find_index(fields, &(&1["id"] == id))

    {idx, Enum.at(fields, idx)}
  end

  defp swap_els(enum, idx1, idx2) do
    el1 = Enum.at(enum, idx1)
    el2 = Enum.at(enum, idx2)

    enum
    |> List.replace_at(idx1, el2)
    |> List.replace_at(idx2, el1)
  end
end
