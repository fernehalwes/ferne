defmodule FerneWeb.AddTeamMembersLive do
  @moduledoc """
  Live view for rendering the view where you can add
  new team members.
  """
  use FerneWeb, :live_view
  alias Ferne.Repo
  alias Ferne.{Team, TeamInvite, User}

  def render(assigns) do
    render(FerneWeb.TeamView, "add_members.live.html", assigns)
  end

  def mount(%{"id" => id}, %{"user_token" => user_token} = _session, socket) do
    team = Team.get_by_id(id) |> Repo.preload([:members, :team_members])
    socket = assign(socket, user_token: user_token)
    user = FerneWeb.UserAuth.fetch_live_user(socket)

    if Bodyguard.permit?(Team, :manage_team, user, team) do
      invites = TeamInvite.get_all_for_team(team) |> Repo.preload([:user])

      {:ok,
       assign(socket,
         team: team,
         current_user: user,
         search_results: [],
         invites: invites,
         page_title: "Add members to #{team.name}"
       )}
    else
      {:ok,
       socket
       |> put_flash(:error, "You're not authorized for this.")
       |> redirect(to: Routes.team_path(socket, :show, id))}
    end
  end

  def handle_event("search", %{"name" => %{"name" => ""}}, socket) do
    {:noreply, assign(socket, search_results: [])}
  end

  def handle_event("search", %{"name" => params}, socket) do
    # Avoid LIKE injections
    sanitized_username = String.replace(params["name"], "%", " ")

    matches =
      User.search_by_username(sanitized_username)
      |> Enum.filter(fn f ->
        !Enum.any?(socket.assigns.team.members, &(&1.id == f.id)) &&
          !Enum.any?(socket.assigns.invites, &(&1.user.id == f.id))
      end)

    {:noreply, assign(socket, search_results: matches)}
  end

  def handle_event("sendInvite", %{"id" => id}, socket) do
    user = Enum.find(socket.assigns.search_results, &(&1.id == id))

    TeamInvite.create_and_send(
      socket.assigns.team,
      user,
      &Routes.team_url(socket, :accept_invite, &1)
    )

    new_invites = TeamInvite.get_all_for_team(socket.assigns.team) |> Repo.preload([:user])

    {:noreply,
     socket
     |> assign(invites: new_invites, search_results: [])
     |> push_event("showToast", %{text: "Invite successfully sent!"})}
  end
end
