defmodule FerneWeb.NewEventLive do
  @moduledoc """
  Live component for creating a new event.
  """
  use FerneWeb, :live_view
  alias Ferne.Event

  def render(assigns) do
    render(FerneWeb.EventView, "new.live.html", assigns)
  end

  def mount(%{"edit" => event_id}, %{"user_token" => user_token} = _session, socket) do
    event = Event.get_by_id(event_id)
    description_preview = if event.description, do: Cmark.to_html(event.description)
    socket = assign(socket, user_token: user_token)
    user = FerneWeb.UserAuth.fetch_live_user(socket) |> Ferne.Repo.preload([:team_memberships])

    {:ok,
     assign(socket,
       changeset: Event.creation_changeset(event, %{}),
       event: event,
       attendee_limit_active: !!event.attendee_limit,
       description_preview: description_preview,
       current_user: user,
       page_title: "Edit #{event.name}",
       edit: true
     )}
  end

  def mount(_params, %{"user_token" => user_token} = _session, socket) do
    socket = assign(socket, user_token: user_token)
    user = FerneWeb.UserAuth.fetch_live_user(socket) |> Ferne.Repo.preload([:team_memberships])

    {:ok,
     assign(socket,
       changeset: Event.creation_changeset(%Event{}, %{}),
       attendee_limit_active: false,
       event: nil,
       description_preview: nil,
       current_user: user,
       page_title: "Create new event",
       edit: false
     )}
  end

  def handle_event("form_change", %{"event" => event_params}, socket) do
    changeset =
      (socket.assigns.event || %Event{})
      |> Event.creation_changeset(event_params)

    IO.inspect(event_params)
    IO.inspect(changeset)

    {:noreply,
     assign(socket,
       attendee_limit_active: event_params["has_attendee_limit"] == "true",
       description_preview: Cmark.to_html(event_params["description"]),
       changeset: changeset
     )}
  end

  def handle_event("form_save", %{"event" => event_params}, socket) do
    if socket.assigns.event do
      case Event.update(socket.assigns.event, event_params) do
        {:ok, _event} ->
          {:noreply,
           socket
           |> put_flash(
             :info,
             "Event successfully updated!"
           )
           |> redirect(to: Routes.event_path(socket, :show_manage, socket.assigns.event.id))}

        {:err, changeset} ->
          {:noreply, assign(socket, changeset: changeset)}
      end
    else
      case Event.create(event_params, socket.assigns.current_user) do
        {:ok, event} ->
          {:noreply,
           socket
           |> put_flash(:info, "Event successfully created!")
           |> redirect(to: Routes.event_path(socket, :show_manage, event.id))}

        {:error, %Ecto.Changeset{} = changeset} ->
          {:noreply, assign(socket, changeset: changeset)}
      end
    end
  end
end
