defmodule FerneWeb.Router do
  use FerneWeb, :router

  import FerneWeb.UserAuth

  pipeline :browser do
    plug :accepts, ["html"]
    plug :fetch_session
    plug :fetch_live_flash
    plug :put_root_layout, {FerneWeb.LayoutView, :root}
    plug :protect_from_forgery
    plug :put_secure_browser_headers
    plug :fetch_current_user
  end

  pipeline :api do
    plug :accepts, ["json"]
  end

  ## Authentication routes

  scope "/", FerneWeb do
    pipe_through [:browser, :redirect_if_user_is_authenticated]

    get "/users/register", UserRegistrationController, :new
    post "/users/register", UserRegistrationController, :create
    get "/users/log_in", UserSessionController, :new
    post "/users/log_in", UserSessionController, :create
    get "/login/:token", UserSessionController, :login
    post "/login/:token", UserSessionController, :post_login
  end

  scope "/", FerneWeb do
    pipe_through [:browser, :require_authenticated_user]

    get "/users/settings", UserSettingsController, :edit
    put "/users/settings", UserSettingsController, :update
    put "/users/delete", UserSettingsController, :delete
    get "/users/settings/confirm_email/:token", UserSettingsController, :confirm_email

    live "/events/editor", NewEventLive
    get "/events/:id/manage", EventController, :show_manage
    live "/events/:id/manage/organizers", ManageEventOrganizersLive
    get "/events/:id/manage/attendees", EventController, :manage_attendees
    post "/events/:id/cancel", EventController, :cancel
    delete "/events/:id/delete", EventController, :delete
    get "/events/:id/toggle_registrations", EventController, :toggle_registrations
    get "/events/:id/toggle_public", EventController, :toggle_public
    get "/events/:id/activate_block/:block", EventController, :activate_block
    get "/events/:id/special/:block", EventController, :block_special_get
    post "/events/:id/special/:block", EventController, :block_special_post
    get "/events/:id/settings/:block", EventController, :block_get
    post "/events/:id/settings/:block", EventController, :block_post
    delete "/events/:id/delete_block/:block", EventController, :block_delete
    get "/events/:id/save", EventController, :new_template
    post "/events/:id/save", EventController, :create_template
    delete "/events/delete_template/:id", EventController, :delete_template
    get "/events/:id/export_sheet", EventController, :export_sheet
    get "/events/:id/export_party_sheet", EventController, :export_party_sheet
    get "/templates/:id/use", EventController, :new_via_template
    post "/templates/:id/use", EventController, :create_via_template

    get "/teams/new", TeamController, :new
    post "/teams/new", TeamController, :create
    get "/teams/:id/edit", TeamController, :edit
    put "/teams/:id/edit", TeamController, :update
    get "/teams/:id/delete", TeamController, :delete
    live "/teams/:id/members/add", AddTeamMembersLive
    get "/teams/:id/toggle_admin/:user_id", TeamController, :toggle_admin
    get "/teams/:id/toggle_superadmin/:user_id", TeamController, :toggle_superadmin
    get "/teams/:id/remove/:user_id", TeamController, :remove_member
    get "/teams/:id/templates", TeamController, :show_templates
    get "/teams/invite/:token", TeamController, :accept_invite

    get "/teams/:id/attention_lists", AttentionListController, :show
    get "/teams/:id/attention_lists/new", AttentionListController, :new
    post "/teams/:id/attention_lists/new", AttentionListController, :create
    get "/attention_lists/:id/delete", AttentionListController, :delete
  end

  scope "/", FerneWeb do
    pipe_through :browser

    get "/", PageController, :index
    get "/frontpage", PageController, :frontpage
    get "/about", PageController, :about
    get "/terms", PageController, :terms
    get "/privacy", PageController, :privacy
    get "/events/:id", EventController, :show_signup
    post "/events/:id", AttendeeController, :create
    get "/edit/:token", AttendeeController, :edit
    put "/edit/:token", AttendeeController, :update
    get "/teams/:id", TeamController, :show
    get "/teams/:id/members", TeamController, :show_members

    # Auth routes
    get "/auth/discord/finish", AuthController, :finish_discord
    get "/auth/discord/:id", AuthController, :start_discord
    delete "/users/log_out", UserSessionController, :delete
    get "/users/confirm", UserConfirmationController, :new
    post "/users/confirm", UserConfirmationController, :create
    get "/users/confirm/:token", UserConfirmationController, :edit
    post "/users/confirm/:token", UserConfirmationController, :update
  end

  # Enables LiveDashboard only for development
  #
  # If you want to use the LiveDashboard in production, you should put
  # it behind authentication and allow only admins to access it.
  # If your application does not have an admins-only section yet,
  # you can use Plug.BasicAuth to set up some basic authentication
  # as long as you are also using SSL (which you should anyway).
  if Mix.env() in [:dev, :test] do
    import Phoenix.LiveDashboard.Router

    scope "/" do
      pipe_through :browser
      live_dashboard "/dashboard", metrics: FerneWeb.Telemetry
    end
  end

  # Enables the Swoosh mailbox preview in development.
  #
  # Note that preview only shows emails that were sent by the same
  # node running the Phoenix server.
  if Mix.env() == :dev do
    scope "/dev" do
      pipe_through :browser

      forward "/mailbox", Plug.Swoosh.MailboxPreview
    end
  end
end
