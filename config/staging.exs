import Config

config :ferne, FerneWeb.Endpoint,
  url: [host: "stage.fernehalwes.org", port: 443, scheme: "https"],
  cache_static_manifest: "priv/static/cache_manifest.json"

config :logger, level: :info, backends: [:console, Sentry.LoggerBackend]
