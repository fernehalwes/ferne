import Config

# config/runtime.exs is executed for all environments, including
# during releases. It is executed after compilation and before the
# system starts, so it is typically used to load production configuration
# and secrets from environment variables or elsewhere. Do not define
# any compile-time configuration in here, as it won't be applied.
# The block below contains prod specific runtime configuration.
if config_env() == :prod || config_env() == :staging do
  database_url =
    System.get_env("DATABASE_URL") || raise "Environment variable DATABASE_URL is missing."

  config :ferne, Ferne.Repo,
    url: database_url,
    socket_options: [:inet6],
    pool_size: String.to_integer(System.get_env("POOL_SIZE") || "10")

  _app_name =
    System.get_env("FLY_APP_NAME") || raise "Environment variable FLY_APP_NAME is missing."

  secret_key_base =
    System.get_env("SECRET_KEY_BASE") || raise "Environment variable SECRET_KEY_BASE is missing."

  postmark_key =
    System.get_env("POSTMARK_API_KEY") ||
      raise "Environment variable POSTMARK_API_KEY is missing."

  sentry_key = System.get_env("SENTRY_DSN") || raise "Environment variable SENTRY_DSN is missing."

  discord_client_id =
    System.get_env("FERNE_DISCORD_CLIENT_ID") ||
      raise "Environment variable FERNE_DISCORD_CLIENT_ID is missing."

  discord_client_secret =
    System.get_env("FERNE_DISCORD_CLIENT_SECRET") ||
      raise "Environment variable FERNE_DISCORD_CLIENT_SECRET is missing."

  discord_redirect_url =
    System.get_env("FERNE_DISCORD_REDIRECT_URL") || "https://fernehalwes.org/auth/discord/finish"

  config :ferne, FerneWeb.Endpoint,
    server: true,
    http: [
      # Enable IPv6 and bind on all interfaces.
      # Set it to  {0, 0, 0, 0, 0, 0, 0, 1} for local network only access.
      # See the documentation on https://hexdocs.pm/plug_cowboy/Plug.Cowboy.html
      # for details about using IPv6 vs IPv4 and loopback vs public addresses.
      ip: {0, 0, 0, 0, 0, 0, 0, 0},
      port: String.to_integer(System.get_env("PORT") || "4000"),
      transport_options: [socket_opts: [:inet6]]
    ],
    secret_key_base: secret_key_base

  config :ferne, :discord_auth,
    client_id: discord_client_id,
    client_secret: discord_client_secret,
    redirect_url: discord_redirect_url

  config :ferne, Ferne.Mailer,
    adapter: Swoosh.Adapters.Postmark,
    api_key: postmark_key

  config :sentry,
    dsn: sentry_key,
    environment_name: :prod,
    enable_source_code_context: true,
    root_source_code_path: File.cwd!(),
    tags: %{
      env: config_env()
    },
    release: Ferne.version(),
    included_environments: [:production]
end
