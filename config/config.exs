# This file is responsible for configuring your application
# and its dependencies with the aid of the Config module.
#
# This configuration file is loaded before any dependency and
# is restricted to this project.

# General application configuration
import Config

config :ferne,
  ecto_repos: [Ferne.Repo],
  generators: [binary_id: true]

# Configures the endpoint
config :ferne, FerneWeb.Endpoint,
  url: [host: "localhost"],
  secret_key_base: "T2NsuVjSxVNbTEdVGPVDAeV2ibhE1N2/Q42XyTFyL7kx8c3YBsIZDOK8nwF9l4UM",
  render_errors: [view: FerneWeb.ErrorView, accepts: ~w(html json), layout: false],
  pubsub_server: Ferne.PubSub,
  live_view: [signing_salt: "6tQ4p3z1"]

# Configures the mailer
#
# By default it uses the "Local" adapter which stores the emails
# locally. You can see the emails in your browser, at "/dev/mailbox".
#
# For production it's recommended to configure a different adapter
# at the `config/runtime.exs`.
config :ferne, Ferne.Mailer, adapter: Swoosh.Adapters.Local

# Configures Elixir's Logger
config :logger, :console,
  format: "$time $metadata[$level] $message\n",
  metadata: [:request_id]

# Use Jason for JSON parsing in Phoenix
config :phoenix, :json_library, Jason

# Import environment specific config. This must remain at the bottom
# of this file so it overrides the configuration defined above.
import_config "#{config_env()}.exs"
