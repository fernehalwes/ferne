FROM elixir:1.13.3-alpine as build

# install build dependencies
RUN apk add --no-cache build-base git python3 curl npm

# prepare build dir
WORKDIR /app

# extend hex timeout
ENV HEX_HTTP_TIMEOUT=20

# install hex + rebar
RUN mix local.hex --force
RUN mix local.rebar --force

# set build ENV
ARG MIX_ENV
ENV MIX_ENV=$MIX_ENV
ENV SECRET_KEY_BASE=nokey

# install mix dependencies
COPY mix.exs mix.lock ./
COPY config config
RUN mix deps.get --only prod && \
    mix deps.compile

# install npm dependencies
COPY package.json package-lock.json ./
RUN npm ci

COPY priv priv
COPY assets assets
RUN mix assets.deploy
RUN mix phx.digest

# compile and build the release
COPY lib lib
COPY rel rel
RUN mix do compile, release

# Start a new build stage so that the final image will only contain
# the compiled release and other runtime necessities
FROM alpine:3.16 AS app
RUN apk add --no-cache libstdc++ openssl ncurses-libs

WORKDIR /app

ARG MIX_ENV
ENV MIX_ENV=$MIX_ENV

RUN chown nobody:nobody /app
USER nobody:nobody

COPY --from=build --chown=nobody:nobody /app/_build/$MIX_ENV/rel/ferne ./

ENV HOME=/app
ENV SECRET_KEY_BASE=nokey
ENV PORT=4000

CMD ["bin/ferne", "start"]
