defmodule Ferne.Repo.Migrations.AddSuperAdminToTeamMembers do
  use Ecto.Migration

  def change do
    alter table(:team_members) do
      add :superadmin, :boolean
    end
  end
end
