defmodule Ferne.Repo.Migrations.AddEventTemplates do
  use Ecto.Migration

  def change do
    create table(:event_templates, primary_key: false) do
      add :id, :binary_id, primary_key: true
      add :name, :string
      add :event_data, :map
      add :block_data, :map
      add :team_id, references(:teams, type: :binary_id)

      timestamps(type: :utc_datetime, updated_at: false)
    end
  end
end
