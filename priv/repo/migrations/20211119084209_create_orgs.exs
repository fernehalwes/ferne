defmodule Ferne.Repo.Migrations.CreateOrgs do
  use Ecto.Migration

  def change do
    create table(:teams, primary_key: false) do
      add :id, :binary_id, primary_key: true
      add :name, :string
      add :description, :string
      add :link, :string
      add :public_members, :boolean

      add :user_id, references(:users, type: :binary_id)
      timestamps(type: :utc_datetime)
    end

    create table(:team_members, primary_key: false) do
      add :id, :binary_id, primary_key: true
      add :team_id, references(:teams, type: :binary_id, on_delete: :delete_all)
      add :user_id, references(:users, type: :binary_id)
      add :admin, :boolean

      timestamps()
    end

    create table(:team_invites, primary_key: false) do
      add :id, :binary_id, primary_key: true
      add :token, :binary

      add :user_id, references(:users, type: :binary_id, on_delete: :delete_all)
      add :team_id, references(:teams, type: :binary_id)
      timestamps(updated_at: false)
    end

    alter table(:events) do
      add :public, :boolean
      add :team_id, references(:teams, type: :binary_id)
    end

    create index(:team_members, [:team_id, :user_id])
    create index(:team_invites, [:user_id, :team_id])
    create unique_index(:team_invites, [:token])
  end
end
