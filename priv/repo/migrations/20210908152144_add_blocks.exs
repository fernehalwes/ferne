defmodule Ferne.Repo.Migrations.AddBlocks do
  use Ecto.Migration

  def change do
    create table("blocks", primary_key: false) do
      add :id, :binary_id, primary_key: true
      add :identifier, :string, null: false
      add :data, :map
      add :event_id, references(:events, type: :binary_id, on_delete: :delete_all), null: false
      timestamps()
    end

    create index(:blocks, [:event_id])
  end
end
