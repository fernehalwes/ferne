defmodule Ferne.Repo.Migrations.AddSelectedToAttendees do
  use Ecto.Migration

  def up do
    execute "CREATE EXTENSION fuzzystrmatch"

    alter table("attendees") do
      add :selected, :boolean
    end
  end

  def down do
    execute "DROP EXTENSION IF EXISTS fuzzystrmatch"

    alter table("attendees") do
      remove :selected
    end
  end
end
