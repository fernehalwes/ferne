defmodule Ferne.Repo.Migrations.AddUsernameToUsers do
  use Ecto.Migration
  import Ecto.Query
  alias Ferne.Repo
  alias Ferne.User

  def up do
    alter table("users") do
      add :username, :string
    end

    create index(:users, [:username])
    flush()

    existing_users = from(u in User, select: u) |> Repo.all()

    Enum.each(existing_users, fn user ->
      # Set everyone's username
      Repo.query!("UPDATE users SET username = $1 WHERE email = $2", [
        Slug.slugify(user.name),
        user.email
      ])
    end)

    alter table("users") do
      modify :username, :string, null: false
    end
  end

  def down do
    alter table("users") do
      remove :username
    end
  end
end
