defmodule Ferne.Repo.Migrations.AddEditTokenToAttendees do
  use Ecto.Migration

  def change do
    alter table("attendees") do
      add :edit_token, :binary
    end

    create index(:attendees, [:edit_token])
  end
end
