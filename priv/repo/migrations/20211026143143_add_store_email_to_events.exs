defmodule Ferne.Repo.Migrations.AddStoreEmailToEvents do
  use Ecto.Migration

  def change do
    alter table("events") do
      add :store_email, :boolean
    end
  end
end
