defmodule Ferne.Repo.Migrations.AddEvents do
  use Ecto.Migration

  def change do
    create table("events", primary_key: false) do
      add :id, :binary_id, primary_key: true
      add :name, :string
      add :description, :string
      add :time, :utc_datetime
      add :location, :string
      add :attendee_limit, :integer
      add :state, :string
      add :user_id, references(:users, on_delete: :delete_all, type: :binary_id), null: false

      timestamps(type: :utc_datetime)
    end

    create table("attendees", primary_key: false) do
      add :id, :binary_id, primary_key: true
      add :name, :string
      add :ign, :string
      add :email, :string
      add :event_id, references(:events, on_delete: :delete_all, type: :binary_id), null: false

      timestamps(type: :utc_datetime)
    end

    create table("event_organizers", primary_key: false) do
      add :id, :binary_id, primary_key: true
      add :event_id, references(:events, on_delete: :delete_all, type: :binary_id), null: false
      add :user_id, references(:users, on_delete: :delete_all, type: :binary_id), null: false
      add :role, :string

      timestamps()
    end
  end
end
