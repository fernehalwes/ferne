defmodule Ferne.Repo.Migrations.AddCustomDataToAttendees do
  use Ecto.Migration

  def change do
    alter table("attendees") do
      add :custom_data, :map
    end
  end
end
