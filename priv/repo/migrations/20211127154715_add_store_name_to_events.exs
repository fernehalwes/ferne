defmodule Ferne.Repo.Migrations.AddStoreNameToEvents do
  use Ecto.Migration
  alias Ferne.Repo

  def up do
    alter table("events") do
      add :store_name, :boolean
    end

    flush()

    all_events = Repo.all(Ferne.Event)

    for event <- all_events do
      changeset = Ferne.Event.creation_changeset(event, %{store_name: true})
      Repo.update(changeset)
    end
  end

  def down do
    alter table("events") do
      remove :store_name
    end
  end
end
