defmodule Ferne.Repo.Migrations.AddAttentionLists do
  use Ecto.Migration

  def change do
    create table(:attention_lists, primary_key: false) do
      add :id, :binary_id, primary_key: true
      add :ign, :string
      add :type, :string
      add :reason, :string
      add :inserted_by, references(:users, type: :binary_id)
      add :team_id, references(:teams, type: :binary_id)

      timestamps(type: :utc_datetime, updated_at: false)
    end
  end
end
