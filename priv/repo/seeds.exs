# Script for populating the database. You can run it as:
#
#     mix run priv/repo/seeds.exs
#
# Inside the script, you can read and write to any of your
# repositories directly:
#
#     Ferne.Repo.insert!(%Ferne.SomeSchema{})
#
# We recommend using the bang functions (`insert!`, `update!`
# and so on) as they will fail if something goes wrong.

import Ecto.Query
Faker.start()

roles = FerneWeb.Roles.all_roles()

user = Ferne.Repo.one!(from(u in Ferne.User, order_by: [asc: u.inserted_at], limit: 1))
exp_uuid = Ecto.UUID.generate()

exp_fields = [
  %{id: Ecto.UUID.generate(), value: "a little"},
  %{id: Ecto.UUID.generate(), value: "a lot"}
]

logos_uuid = Ecto.UUID.generate()

logos_fields = [
  %{id: Ecto.UUID.generate(), value: "bravery"},
  %{id: Ecto.UUID.generate(), value: "dispel"}
]

team_uuid = Ecto.UUID.generate()

evt =
  Ferne.Repo.insert!(%Ferne.Event{
    name: Faker.Vehicle.standard_spec(),
    description: "auto-seeded event",
    time: Timex.add(Timex.now(), Timex.Duration.from_days(30)) |> DateTime.truncate(:second),
    location: Faker.Address.country(),
    has_attendee_limit: 60,
    user_id: user.id
  })

roles_block =
  Ferne.Repo.insert!(%Ferne.Block{
    identifier: "ffxiv_roles",
    event_id: evt.id,
    data: %{}
  })

cf_block =
  Ferne.Repo.insert!(%Ferne.Block{
    identifier: "custom_fields",
    event_id: evt.id,
    data: %{
      "data" => [
        %{
          id: exp_uuid,
          name: "What is your current BA experience?",
          type: "single_select",
          choices: exp_fields,
          has_form: true
        },
        %{
          id: logos_uuid,
          name: "Are there any logos actions you already have crafted in your logos inventory?",
          type: "multi_select",
          choices: logos_fields,
          has_form: true
        },
        %{
          id: team_uuid,
          name: "Team / friend name?",
          type: "string",
          has_form: true
        }
      ]
    }
  })

Enum.each(1..60, fn _ ->
  Ferne.Repo.insert!(%Ferne.Attendee{
    name: Faker.Person.name(),
    ign: "#{Faker.Person.first_name()} #{Faker.Person.last_name()}",
    email: Faker.Internet.email(),
    selected: false,
    custom_data: %{
      "ffxiv_roles" => Enum.map(1..3, fn _ -> Enum.random(roles) end) |> Enum.join(","),
      exp_uuid => Enum.random(exp_fields).id,
      team_uuid =>
        [Faker.Person.first_name()] |> Enum.concat(["", "", "", "", ""]) |> Enum.random()
    },
    event_id: evt.id
  })
end)
